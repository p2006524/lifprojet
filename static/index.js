const express = require('express');
const app = express();
const request = require('request');
const { spawn } = require('child_process');
const bodyParser = require('body-parser');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const fs = require("fs");
const { Console } = require('console');
const path = require('path');

var user = null ;
let dataStatsUser = "";

app.listen(8080, () => console.log('Application listening on port 8080!'))

app.use(bodyParser.urlencoded({ extended: true }));

//permet de récupérer l'image et l'envoyer a express
app.get('/img/ACPKMeans_prediction_Knn.png',(req , res ) =>{
  res.sendFile(path.join(__dirname,  '../img/ACPKMeans_prediction_Knn.png'))
});
app.get('/img/ACPKMeans_prediction_Svd.png',(req , res ) =>{
  res.sendFile(path.join(__dirname,  '../img/ACPKMeans_prediction_Svd.png'))
});
app.get('/img/CAHcomparaison.png',(req , res ) =>{
  res.sendFile(path.join(__dirname,  '../img/CAHcomparaison.png'))
});
app.get('/img/determinerQ.png',(req , res ) =>{
  res.sendFile(path.join(__dirname,  '../img/determinerQ.png'))
});
app.get('/img/deterCoude.png',(req , res ) =>{
  res.sendFile(path.join(__dirname,  '../img/deterCoude.png'))
});
app.get('/img/hybride_average.png',(req , res ) =>{
  res.sendFile(path.join(__dirname,  '../img/hybride_average.png'))
});
app.get('/img/CompareSelectGenre.png',(req , res ) =>{
  res.sendFile(path.join(__dirname,  '../img/CompareSelectGenre.png'))
});
app.get('/img/CompareEvalGenre.png',(req , res ) =>{
  res.sendFile(path.join(__dirname,  '../img/CompareEvalGenre.png'))
});


//permet de récupérer le script js et l'envoyer a express
app.get('/script.js', (req, res) => {
  res.type('text/javascript');
  res.sendFile(path.join(__dirname, 'script.js'));
});

//route de base
app.get('/', (req, res) => {
    const html = fs.readFileSync("./index.html", "utf8");
    const css = fs.readFileSync('./newIndex.css', 'utf8');
    const htmlWithCss = `<html><head><title>QssQss</title><style>${css}</style></head><body>${html}</body></html>`;
    res.send(htmlWithCss);
})

//route vers le formulaire pour le top
app.get('/top', (req, res) => {
  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<!doctype html><html><head><title>QssQss</title><style>${css}</style></head><body>${html}</body></html>`;

  const dom = new JSDOM(htmlWithCss);
  const body = dom.window.document.getElementById("container");

  const form=`
    <h2>Quelle genre de film ? </h2>
    <div class="column">
      <form action="/getInfo" method="POST">
        <!-- <div class="Element">
          <label class="label"  for="base">Base :</label>
          <input class="Saisie" type="text" id="base" name="base"><br>
        </div> -->
        <div class="Element">
          <label class="label"  for="legenre">Le Genre voulu :</label>
          <select class="Saisie" name="legenre" id="legenre">  
          </select><br>
        </div>
        <!-- <div class="Element">
          <label class="label" for="colonneTri">La colonne a trier :</label>
          <select class="Saisie" name="colonneTri" id="colonneTri">  
            <option value="rating">Note</option>
            <option value="vote">Nbr de Vote</option>
          </select><br>
        </div> -->
        <div class="Element">
          <label class="label" for="x">Combien :</label>
          <input class="Saisie" type="text" id="x" name="x"><br>
        </div>
        <button class="Envoyer" type="submit">Envoyer</button>
      </form>
    </div>
  `;
    
  body.innerHTML=form;
  const select = dom.window.document.getElementById("legenre");    

  //fonction python pour récupérer tout les genres
  const pyProg = spawn('python', ['../py/analyseArthur.py','extractGenre']);

  let data="";
  pyProg.stdout.on('data', function(chunk) {
    data+=chunk;
  });

  pyProg.stdout.on('end', function() {

    let options=data.toString();
    //on récup une liste donc on retire tout les éléments qu'on ne veut pas afficher [ ; ] ; , 
    options=options.split(/,|\[|\]/);
    for (let i = 1; i < options.length-1; i++) {
      const option = dom.window.document.createElement("option");
      option.text = options[i];
      option.value = options[i];
      select.appendChild(option);    
    }
    if(user!=null){

      const div = dom.window.document.createElement("div");
      div.id="divImg";
      div.className="divImg";

      const img = dom.window.document.createElement("img");
      img.src="./img/hybride_average.png";
      const img2 = dom.window.document.createElement("img");
      img2.src="./img/CompareSelectGenre.png";
      const img3 = dom.window.document.createElement("img");
      img3.src="./img/CompareEvalGenre.png";


      const p = dom.window.document.createElement("p");
      p.innerHTML=dataStatsUser;
      p.className="white-space";
      body.appendChild(p);

      div.appendChild(img);
      div.appendChild(img2);
      div.appendChild(img3);

      body.appendChild(div);

      const outputHtml = dom.serialize();
      res.send(outputHtml);
    }else{
      const outputHtml = dom.serialize();
      res.send(outputHtml);
      }
    });
})

//route qui récupére les infos du formulaire top
app.post('/getInfo', (req, res) => {
  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<!doctype html><html><head><title>QssQss</title><style>${css}</style></head><body>${html}</body></html>`;

  const dom = new JSDOM(htmlWithCss);
  const bod = dom.window.document.getElementById("container");

  //const base = req.body.base;
  const legenre = req.body.legenre;
  //const colonneTri = req.body.colonneTri;
  const x = req.body.x;

  //fonction python pour avoir les x meilleurs films pour un genre
  // const pyProg = spawn('python', ['../py/analyseArthur.py','lesPremiersPrGenre',base,legenre,colonneTri,x]);
  const pyProg = spawn('python', ['../py/analyseArthur.py','top',legenre,x]);

  let html2=`  
      <h1>Voici ce qu'on te recommande</h1>
  `;
    
  pyProg.stdout.on('data', function(data) {
    html2+='<p class="white-space">';
    myString=data.toString();
    html2 += myString;
    html2+='</p>';
    html2+='<a href="/"><button class="Envoyer">Retour accueil</button></a>';
    html2+='</body>';
    bod.innerHTML=html2;
    const outputHtml = dom.serialize();
    res.send(outputHtml);
  });
})

//route inutile
/*
app.get('/getValeur', (req, res) => {
  const pyProg = spawn('python', ['../py/analyseArthur.py']);

  let html=`  
    <html>
    <head>
      <title>Recommandation</title>
      <style>
        .white-space{
          white-space : pre-wrap;
        }
      </style>

    </head>

      <body>
        <h1>Voici la réponse</h1>
      `;


  pyProg.stdout.on('data', function(data) {
    //console.log(data.toString());
    // var myArray = myNumber.toString().split('');

    // // Reconstitution de la chaîne avec des balises <br>
    // var myString = myArray.join('<br>');

    // // Affichage de la variable dans la page HTML
    // document.getElementById('myVariable').innerHTML = myString;

    html+='<p class="white-space">';
    myString=data.toString();
    newString=myString.replace(' ', '\n');
    html += newString;
    html+='</p>';
    res.send(html);

  });

});*/

//route vers la liste de film
app.get('/liste', (req, res) => {
  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<html><head><title>QssQss</title><style>${css}</style></head><body>${html}<script src="script.js"></script></body></html>`;
  const dom = new JSDOM(htmlWithCss);

  const body = dom.window.document.getElementById("container");
  const form= `
    <label class="label" for="chercher">Chercher :</label>
    <input type="text" id="chercher" class="Forminput">
    <table id="liste">
    </table>
    <button id="showMoreButton" class="Envoyer">Afficher plus</button>
  `;
  body.innerHTML=form;
  //fonction python qui récupére tout les films de la BD
  const pyProg = spawn('python', ['../py/analyseArthur.py', 'liste']);

  //variable pour stocker ces films
  let data = "";
  pyProg.stdout.on('data', function(chunk) {
    data += chunk;
  });

  //variable pour stocker les films que l'utilisateur user a aimé
  let datauser="";
  if(user != null){
    //fonction python qui récupére tout les films de la BD que l'utilisateur user a aimé
    const pyProg2 = spawn('python', ['../py/analyseArthur.py', 'detailliste' , user ]);
    pyProg2.stdout.on('data', function(chunk) {
      datauser += chunk;
    });
  }

  //une fois qu'on a finis de stocker tout les films on veut les afficher
  pyProg.stdout.on('end', function() {
    const allData = JSON.parse(data);
    //on affiche les 9 premiers
    let pageData = allData.slice(0, 9);
    const tableBody = dom.window.document.getElementById('liste');

    const first = dom.window.document.createElement('tr');
    first.innerHTML =`<tr>
    <th>Nom</th>
    <th>Description</th>
    <th>Vote moyenne (sur 10)</th>
    </tr>`;

    if(user != null){
      first.innerHTML =`<tr>
      <th>Nom</th>
      <th>Description</th>
      <th>Vote moyenne (sur 10)</th>
      <th>Aimer/Votre note (sur 5)</th>
      </tr>
      `;
    }

    tableBody.appendChild(first);

    pageData.forEach(row => {
      const tr = dom.window.document.createElement('tr');
      tr.innerHTML = `
        <td>${row.title}</td>
        <td>${row.overview}</td>
        <td>${row.vote_average}</td>
        `
      ;
      if(user != null){
        let trouver = false;
        let note = 0;
        const allDataUser=JSON.parse(datauser);
        allDataUser.forEach(recu =>{
          if (recu.title==row.title) {
            trouver=true
            note = recu.rating
          }
        })
        if(trouver){
          tr.innerHTML += `
          <td>${note}</td>
        `;
        }else{
          tr.innerHTML += `<td><a href='/rating?title=${encodeURIComponent(row.title)}'>J'aime</a></td>`;
        }
      }     
      tableBody.appendChild(tr);
    });

    const outputHtml = dom.serialize();
    res.send(outputHtml);
  });
});

//route vers le système de connexion
app.get('/connexion', (req, res) => {
  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<html><head><title>QssQss</title><style>${css}</style></head><body>${html}</body></html>`;
  const dom = new JSDOM(htmlWithCss);

  const body = dom.window.document.getElementById("container");
  const form= `
    <div class="column">
      <form action="/postConnexion" method="POST">
        <div class="Element">
          <label class="label"  for="utilisateur">Quel utilisateur :</label>
          <!-- <input type="text" id="legenre" name="legenre"><br> -->
          <select class="Saisie" name="utilisateur" id="utilisateur">  
            <!-- <option value="r">r</option> -->
          </select><br>
        </div>
        <button class="Envoyer" type="submit">Envoyer</button>
      </form>
    </div>
  `;
  body.innerHTML=form;

  const select = dom.window.document.getElementById("utilisateur");    
  //fonction qui récupére tout les utilisateurs qui existent
  const pyProg = spawn('python', ['../py/analyseArthur.py', 'listeUtilisateur']);
  pyProg.stdout.on('data', function(data) {

    let options=data.toString();
    //on récup une liste donc on retire tout les éléments qu'on ne veut pas afficher [ ; ] ; , 
    options=options.split(/,|\[|\]/);
    
    for (let i = 1; i < options.length - 1; i++) {
      // console.log(oofed[i]);
      options[i] = options[i].replace(/'/g, '');
      const option = dom.window.document.createElement("option");
      option.text = options[i];
      option.value = options[i];
      select.appendChild(option);    
    }

    const outputHtml = dom.serialize();
    res.send(outputHtml);
  });
})

app.get('/deconnexion' , (req ,res)=>{
  user = null;
  dataStatsUser="";
  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<html><head><title>QssQss</title><style>${css}</style></head><body>${html}</body></html>`;
  const dom = new JSDOM(htmlWithCss);

  const body = dom.window.document.getElementById("container");
  const rep= `
    <h1>Tu es déconnecter</h1>
    <a href="/"><button class="Envoyer">Retour accueil</button></a>
  `;
  body.innerHTML=rep;
  const outputHtml = dom.serialize();
  res.send(outputHtml);
})

//Récupére la valeur du formulaire de /connexion
app.post('/postConnexion', (req, res) => {
  //la variable globale user est mise a jour
  user = req.body.utilisateur;

  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<html><head><title>QssQss</title><style>${css}</style></head><body>${html}</body></html>`;
  const dom = new JSDOM(htmlWithCss);

  const body = dom.window.document.getElementById("container");

  const pyProg = spawn('python', ['../py/analyseArthur.py','statUser',user]);

  pyProg.stdout.on('data', function(chunk) {
    dataStatsUser+=chunk;
  });

  let rep=`  
    <h1>Tu es utilisateur : ${user} </h1>
    <a href="/"><button class="Envoyer">Retour accueil</button></a>
  `;
  body.innerHTML=rep;
  const outputHtml = dom.serialize();
  res.send(outputHtml);
})

//route vers la page de notation
app.get('/rating', (req, res) => {
  const title = req.query.title;

  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<html><head><title>QssQss</title><style>${css}</style></head><body>${html}<script>
  function validateForm() {
    const rating = document.getElementById("rating").value;
    if (rating < 0 || rating > 5) {
      alert("La note doit être comprise entre 0 et 5 !");
      return false;
    }
    return true;
  }
  </script></body></html>`;
  const dom = new JSDOM(htmlWithCss);

  const body = dom.window.document.getElementById("container");

  let rep=`
    <form method="POST" action="/save-rating" onsubmit="return validateForm()"> 
      <label class="label" for="rating">Votre note (sur 5) :</label>
      <input type="text" id="rating" name="rating" class="Forminput"><br>
      <input type="hidden" id="title" name="title" value="${title}"><br>          
      <button type="submit" class="Envoyer">Envoyer</button>
    </form>
    `;
  body.innerHTML=rep;

  const outputHtml = dom.serialize();
  res.send(outputHtml);
});

//Récupére la valeur du formulaire de /rating
app.post('/save-rating', (req, res) => {
  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<!doctype html><html><head><title>QssQss</title><style>${css}</style></head>${html}</html>`;
  const dom = new JSDOM(htmlWithCss);

  const body = dom.window.document.getElementById("container");

  //on recup la note en remplacant les , et ; par .
  const rating = req.body.rating.replace(/[;,]/g, ".");
  //date de la notation
  const time = Date.now();
  const title = req.body.title;

  const rep=`
  <a>Votre note pour ${title} a été ajouté</a>
  </br>
  <a href="/"><button class="Envoyer">Retour accueil</button></a>
  `;
  body.innerHTML=rep;

  //variable qui correspond a l'id pour le film
  let id = null ;

  //fonction python qui récupere l'id pour un titre de film
  const pyProg = spawn('python', ['../py/analyseArthur.py', 'id_links_Par_Titre' , title ]);

  //on stocke l'id
  pyProg.stdout.on('data', function(data) {
    data=data.toString();
    id=data;
  });

  //quand la fonction est fini
  pyProg.stdout.on('end', function() {
    //fonction python qui stocke dans le fichier csv la note
    const pyProg2 = spawn('python', ['../py/analyseArthur.py', 'writeInCsv' , user , id , rating , time ]);
    pyProg2.stdout.on('end' , function(){
      const outputHtml = dom.serialize();
      res.send(outputHtml);
    })
  });
  
})

//route vers la page avec les statistique /\ seulement accessible si user != null
// app.get('/stats' ,(req ,res ) => {
//   const html = fs.readFileSync("./temp.html", "utf8");
//   const css = fs.readFileSync('./style.css', 'utf8');
//   const htmlWithCss = `<!doctype html><html><head><title>QssQss</title><style>${css}</style></head>${html}</html>`;
//   const dom = new JSDOM(htmlWithCss);

//   const body = dom.window.document.getElementById("container");

//   let rep=`
//     <div id="divImg" class ="divImg">
//     </div>
//     `;
//   body.innerHTML=rep;

//   //fonction python qui dessine un graph pour un utilisateur donné
//   const pyProg = spawn('python' , ['../py/analyseArthur.py' , 'plot' , user]);
//   pyProg.stdout.on('end' , function(){
//     //on créé un élément img
//     const img = dom.window.document.createElement("img");
//     //lien vers l'image
//     img.src="./img/ComprendreBudget.png";
//     const divImage = dom.window.document.getElementById("divImg");
//     //on l'ajoute sur la page
//     divImage.appendChild(img);
//     const outputHtml = dom.serialize();
//     res.send(outputHtml);
//   })
// })

app.get('/formRecommandation' , (req , res) => {
  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<!doctype html><html><head><title>QssQss</title><style>${css}</style></head>${html}</html>`;
  const dom = new JSDOM(htmlWithCss);

  const body = dom.window.document.getElementById("container");

  let rep=`
    <form action="/recommandation" method="POST">
      <div class="Element">
        <label class="label" for="reco">Quelle méthode utiliser ?</label>
        <select class="Saisie" name="reco" id="reco">  
          <option value="SIM">SIM</option>
          <option value="KNN">KNN</option>
          <option value="SVD">SVD</option>
        </select><br>
      </div>
      <button class="Envoyer" type="submit">Envoyer</button>
    </form>
    `;
  body.innerHTML=rep;
  const outputHtml = dom.serialize();
  res.send(outputHtml);
})

//route vers la page de recommmandation pour un utilsateur /\ seulement accessible si user != null
app.post('/recommandation', (req, res) => {
  const choix = req.body.reco ;
  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<html><head><title>QssQss</title><style>${css}</style></head><body>${html}</body></html>`;
  const dom = new JSDOM(htmlWithCss);

  const body = dom.window.document.getElementById("container");

  let rep=`
    <table id="liste">

    </table>
    `;
  body.innerHTML=rep;

  //fonction python qui calcule les meilleurs films pour un utilisateur
  const pyProg = spawn('python' , ['../py/analyseArthur.py' , 'recommandation' , choix , user]);

  //variable pour stocker tout les films
  let data = "";
  pyProg.stdout.on('data', function(chunk) {
    data += chunk;
  });

  //variable pour stocker les films que l'utilisateur user a aimé
  let datauser="";
  if(user != null){
    //fonction python qui récupére tout les films de la BD que l'utilisateur user a aimé
    const pyProg2 = spawn('python', ['../py/analyseArthur.py', 'detailliste' , user ]);
    pyProg2.stdout.on('data', function(chunk) {
      datauser += chunk;
    });
  }

  //même système d'affichage que /liste
  pyProg.stdout.on('end', function() {
    const allData = JSON.parse(data);

    const tableBody = dom.window.document.getElementById('liste');

    let first = dom.window.document.createElement('tr');

    first.innerHTML =`<tr>
    <th>Nom</th>
    <th>Description</th>
    <th>Vote moyenne (sur 10)</th>
    </tr>`;

    if(user != null){
      first.innerHTML =`<tr>
      <th>Nom</th>
      <th>Description</th>
      <th>Vote moyenne (sur 10)</th>
      <th>Aimer/Votre note (sur 5)</th>
      </tr>
      `;
    }

    tableBody.appendChild(first);

    allData.forEach(row => {
      const tr = dom.window.document.createElement('tr');
      tr.innerHTML = `
        <td>${row.title}</td>
        <td>${row.overview}</td>
        <td>${row.vote_average}</td>
        `
      ;
      if(user != null){
        let trouver = false;
        let note = 0;
        const allDataUser=JSON.parse(datauser);
        allDataUser.forEach(recu =>{
          if (recu.title==row.title) {
            trouver=true
            note = recu.rating
          }
        })
        if(trouver){
          tr.innerHTML += `
          <td>${note}</td>
        `;
        }else{
          tr.innerHTML += `<td><a href='/rating?title=${encodeURIComponent(row.title)}'>J'aime</a></td>`;
        }
      }         
      tableBody.appendChild(tr);
    });

    const outputHtml = dom.serialize();
    res.send(outputHtml);
  });
});

app.get('/formFilm', (req,res)=>{

  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<html><head><title>QssQss</title><style>${css}</style></head><body>${html}</body></html>`;
  const dom = new JSDOM(htmlWithCss);

  const body = dom.window.document.getElementById("container");

  let rep=`
  <form action="/film" method="POST">
    <div class="Element">
      <label class="label" for="title">Title :</label>
      <input class="Saisie" type="text" id="title" name="title"><br>
    </div>
    <div class="Element">
      <label class="label" for="description">Description :</label>
      <input class="Saisie" type="text" id="description" name="description"><br>
    </div>
    <button class="Envoyer" type="submit">Envoyer</button>
  </form>
    `;
  body.innerHTML=rep;
  const outputHtml = dom.serialize();
  res.send(outputHtml);
})

app.post('/film', (req, res) => {
  const html = fs.readFileSync("./temp.html", "utf8");
  const css = fs.readFileSync('./style.css', 'utf8');
  const htmlWithCss = `<html><head><title>QssQss</title><style>${css}</style></head><body>${html}</body></html>`;
  const dom = new JSDOM(htmlWithCss);

  const body = dom.window.document.getElementById("container");

  const pyProg = spawn('python', ['../py/analyseArthur.py', 'nouveauFilm', req.body.title , req.body.description]);
  let genre="";
  pyProg.stdout.on('data', function(data) {
    genre=data;

    let rep=`
    <p>Voici les genres trouvés ${genre}</p>
    <a href=/><button class="Envoyer">Retour accueil</button></a>
      `;
    body.innerHTML=rep;
  
  
    pyProg.stdout.on('end', function(data) {
      const outputHtml = dom.serialize();
      res.send(outputHtml);
    });
  });
});

//utiliser pour envoyer des données notamment a scripts.js
app.get('/data', (req, res) => {
  const pyProg = spawn('python', ['../py/analyseArthur.py', 'liste']);
  let data = "";
  pyProg.stdout.on('data', function(chunk) {
    data += chunk;
  });

  pyProg.stdout.on('end', function() {
    data = JSON.parse(data);
    res.send(JSON.stringify(data));
  });
});

//utiliser pour envoyer des données notamment a scripts.js
app.get('/user', (req, res) => {
  console.log("je usis le get");
  console.log(user);
  res.send(JSON.stringify(user));
});

//utiliser pour envoyer des données notamment a scripts.js
app.get('/datauser', (req, res) => {
  const pyProg = spawn('python', ['../py/analyseArthur.py', 'detailliste', user]);
  let data = "";
  pyProg.stdout.on('data', function(chunk) {
    data += chunk;
  });

  pyProg.stdout.on('end', function() {
    data = data.toString();
    res.send(JSON.stringify(data));
  });
});



// app.get('./img/*', (req, res) => {
//     res.sendFile(req.url, {root: './'})
// });