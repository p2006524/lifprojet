const tableBody = document.getElementById('liste');
let data = [];
let dataUser =[];
var user = null ;
let nbrDeLigneDejaAfficher = 0;

// Fonction pour ajouter des lignes à la table
function ajouteLigne(start, end) {
  //boucle qui parcours seulement entre index début et fin , on vérifie aussi qu'on dépasse pas la taille de data
  for (let i = start; i < end && i < data.length; i++) {
    // Vérifier si la ligne a déjà été affichée
    if (i < nbrDeLigneDejaAfficher) {
      continue;
    }
    const row = data[i];
    //on crée une tr par ligne a afficher
    const tr = document.createElement('tr');
    tr.innerHTML = `
      <td>${row.title}</td>
      <td>${row.overview}</td>
      <td>${row.vote_average}</td>
    `;
    if(user != null){
      //variable savoir si le film a déjà était liké
      let trouver = false;
      let note = 0;
      const allDataUser=JSON.parse(dataUser);
      allDataUser.forEach(recu =>{
        //si correspond alors déjà liké
        if (recu.title==row.title) {
          trouver=true
          note = recu.rating
        }
      })
      //si déjà liké on affiche la note mit par l'utilisateur
      if(trouver){
        tr.innerHTML += `
        <td>${note}</td>
      `;
      //sinn il peut liké
      }else{
        tr.innerHTML += `<td><a href='/rating?title=${encodeURIComponent(row.title)}'>J'aime</a></td>`;
      }
    }            
    tableBody.appendChild(tr);
  }
  nbrDeLigneDejaAfficher = end;
}

// Récupérer les données JSON de tout les films
fetch('/data')
  .then(response => response.json())
  .then(json => {
    data = json;
    ajouteLigne(9, 10); // Ajoute 1 ligne
  });

// Récupérer l'utilisateur
fetch('/user')
  .then(response => response.json())
  .then(json => {
    user = json;
  });
// Récupérer les données JSON des films déjà liké
fetch('/datauser')
  .then(response => response.json())
  .then(json => {
    dataUser = json;
  });

// Ajouter des lignes supplémentaires en cliquant sur le bouton "Afficher plus"
const bouttonAjout = document.getElementById('showMoreButton');
let nombreDeLigneaAjouter = 10;
bouttonAjout.addEventListener('click', () => {
  ajouteLigne(nombreDeLigneaAjouter, nombreDeLigneaAjouter + 10);
  nombreDeLigneaAjouter += 10;
});

// Chercher des lignes qui correspondent au texte tapé dans l'input
const chercherInput = document.getElementById('chercher');
chercherInput.addEventListener('keydown', (event) => {
  //si on appuie sur entrée 
  if(event.keyCode == 13){
    const searchText = chercherInput.value.toLowerCase();
    tableBody.innerHTML = '';

    let first =`<tr>
    <th>Nom</th>
    <th>Description</th>
    <th>Vote moyenne (sur 10)</th>
    </tr>
    `;

    if(user != null){
      first =`<tr>
      <th>Nom</th>
      <th>Description</th>
      <th>Vote moyenne (sur 10)</th>
      <th>Aimer/Votre note (sur 5)</th>
      </tr>
      `;
    }

    tableBody.innerHTML=first; 

    //trim pour retirer les espaces vides , si vide alors on affiche les 10 premiers films
    if (searchText == null || searchText.trim() == '') {
      nbrDeLigneDejaAfficher = 0;
      tableBody.innerHTML = '';
      tableBody.innerHTML=first; 
      bouttonAjout.style.display = 'inline';
      ajouteLigne(0, 10);
      nombreDeLigneaAjouter = 10;
      return;
    }else if (searchText != null || searchText.trim() != '')
    {
      bouttonAjout.style.display = 'none';
    }

    //comme pour ajouteLigne
    for (let i = 0; i < data.length; i++) {
      const row = data[i];
      const title = row.title.toLowerCase();

      if (title.includes(searchText)) {
        const tr = document.createElement('tr');
        tr.innerHTML = `
          <td>${row.title}</td>
          <td>${row.overview}</td>
          <td>${row.vote_average}</td>
        `;
        if(user != null){
          let trouver = false;
          let note = 0;
          const allDataUser=JSON.parse(dataUser);
          allDataUser.forEach(recu =>{
            if (recu.title==row.title) {
              trouver=true
              note = recu.rating
            }
          })
          if(trouver){
            tr.innerHTML += `
            <td>${note}</td>
          `;
          }else{
            tr.innerHTML += `<td><a href='/rating?title=${encodeURIComponent(row.title)}'>J'aime</a></td>`;
          }
        }       
        tableBody.appendChild(tr);
      }
    }
  }
});
