# Lifproj
Notre projet consiste a créé site internet permetant a un utilisateur d'être recommandé sur des films.

# Conda 
## Pour tous les OS (pour windows il faut être administrateur)
### installer conda
https://www.anaconda.com/products/distribution

### permet de modifié votre shell
si vous voulez utiliser conda dans votre shell :

#### conda init
### pour désactiver l'activation automatique
De base conda s'active. Au lancement votre shell peut mettre plus de temp vous pouvez empechez cela

#### conda config --set auto_activate_base false 
### pour créé un environement avec une version de python
Nous nous utilisons la version 3.10 

#### conda create -n <le nom de l'environement> python=3.10
Permet d'activer l'environement

#### conda activate le nom de l'environement"

# Comment installer toutes les bibliothèques:
# python
/!\ pour se projet seul la version 3.10 de python a été tester.
pip install -r requirements.txt
# JS
apt-get install nodejs
npm install jsdom

# Lancer le site en localhost
pour lancer le site en local vous devez être dans le dossier static et faire : node index.js

# Accéder au site web
http://impie.me:8080/

## QssQss
Recommandation de film.

## Description
Le but de ce projet est de s'initier à de la recomandation de contenu.
Nous utilisons pour nous entrainer une base de donnée de plus de 45.000 films, récupérer via un projet sur Kaggel.
Pour s'entrainer a du machine learning nous avons déveloper différents moyen de faire de la prédiction.
Nous utilisons SVD et KNNBasic via la bibliothèque "surprise". Ainsi que des algorithme fais maison pour mieux comprendre comment fonctionne le machine learning.

## Les exécutables

# analyseArthur
Fichier principal qui permet de simplifier l'execution des autres fonction python sur le site

# analyseAllan
Fichier qui contient toutes les fonctions liées a la reconnaissance de genres avec une fonction mainTrouverGenre qui va faire un boucle sur une liste de films pour remplir et créer une base qui contiendra des mots et des genres associés avec une fréquence, elle va appeler une fonction genreAjouter2 qui va modifier la base avec un mot associé a un genre mis en paramètres. 
Contient aussi une fonction genreTrouver qui va faire a partir d'une description va faire une boucle pour vérifier les mots. Enfin, la dernière fonction importante genreAmeliorer, permet de faire évoluer la base de donnée créé pour qu'elle reconnaisse le genre qui est proposé pour une description.

# AnalyseData

# AnalysePE
Fichier brouillions pour pouvoir expérimenter des fonction de surprise, il y a aussi des tentatives de faire du multithreading.
Pour mieux comprendre et utiliser les algos de recomandation il faut mieux utiliser filtrage_actif. Les fonctions y sont mis au propre et compléter par d'autre moyen de faire de la prédiction.

# dataFrame

# filtrage_actif
Fichier qui permet d'uttiliser des algorithmes de prédictions, il y a plusieurs fonctionnalité qui tourne autour de ces algorithme.
Par exemple il y a l'évaluation de différents algoritme.
En plus de l'utilisartion de surprise, il y a des algorithme "fait maison" et de l'analyse de donnée.

# GenImage

# recherche_bd
Fichier qui permet de plus facilment utiliser plus facilement la base de donné récupérer.
La base de donnée présentes des defauts qui peuvent empecher le bon fonctionnnement de certainne fonction

# system
Fichier qui permet de plus facilement de naviguer dans les dosiers du projets.
Permet un eplus grande flexibilité sur l'endrois ou on déposse nos base de donnée.
Il a été créé en prévisoin d'une adaptabilité pour de futur archive. 


## Doisier static
contient les fichiers js , html  et css donc tout ce qu'il faut pour le site web

# static.js
Ce fichier seulement pour /liste ( sur le site)  , il permet le traitement du bouton afficher plus et chercher un film

# index.js 
fichier serveur ils contient toutes les routes vers les différents onglets du site


