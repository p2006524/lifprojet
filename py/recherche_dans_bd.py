from system import *
import pandas as pd
import json


def id_links_Par_Titre(nom_Du_Film: str, Df_links=None, Df_title=None):
    """
    permet de trouver l'identifiant d'un film a partir de son titre. Compatible avec movies/movies01 ou the-movies-dataset
    attention l'id est celui de links
    """
    if Df_links is None:
        Df_links = pd.read_csv(CheminUnFichier('the-movie/links_sm'), usecols=['movieId', 'tmdbId'])
    if Df_title is None:
        Df_title = pd.read_csv(CheminUnFichier('the-movie/movies_meta'), usecols=['title', 'id'],low_memory=False)
    if not {'movieId', 'tmdbId'}.issubset(set(Df_links.columns)) or not {'title', 'id'}.issubset(set(Df_title.columns)):
        return None
    matching_id = Df_links.loc[Df_links['tmdbId'] == float(Df_title.loc[Df_title['title'] == nom_Du_Film, 'id'].iloc[0])].iloc[0]
    return int(matching_id['movieId'])

def titre_Par_ID_Film(id_du_film: int, df): 
    """
    permet de trouver le titre d'un film a partir de son identifiant. 
    Compatible avec movies/movies01 ou the-movies-dataset
    /!\ l'id doit être celui de movie_metadata
    retourne le titre du film
    """
    if df.empty:
        # print("df.empty")
        return None
    if 'movieId' not in df.columns:
        # print("colone manquante")
        return None
    matching_id = df['movieId'] == id_du_film
    if not matching_id.any():
        # print("not matching_id")
        return None
    movie_title = df.loc[matching_id, 'title'].iloc[0]
    return movie_title

def join_Two_File (link_df, movie_metadata_df):
    """
    permet de joindre fichier deux fichiers du type links et movie_metadata
    retourne une df avec id de links, id de movie_metadata, et les titres des films (pas l'original)
    """
    link_df['tmdbId'] = link_df['tmdbId'].fillna(0).astype(int)
    link_df['tmdbId'] = link_df['tmdbId'].astype(str)
    movie_metadata_df['id'] = movie_metadata_df['id'].astype(str)
    merged_df = pd.merge(left=link_df, right=movie_metadata_df, left_on='tmdbId', right_on='id')
    return merged_df[['movieId', 'tmdbId', 'title']]

def liste_detail_flim_note (utili : int, Df_ratings = None,Df_links = None, Df_movie_metadata = None):
    if Df_ratings is None:
        Df_ratings = pd.read_csv(CheminUnFichier('the-movie/ratings_sm'), usecols=['userId', 'movieId', 'rating'])
    if Df_links is None:
        Df_links = pd.read_csv(CheminUnFichier('the-movie/links_sm'), usecols=['movieId', 'tmdbId'])
    if Df_movie_metadata is None:
        Df_movie_metadata = pd.read_csv(CheminUnFichier('the-movie/movies_meta'),low_memory=False)
    user_ratings = pd.merge(Df_links, Df_ratings[Df_ratings['userId']==utili], on='movieId')
    user_ratings ['tmdbId'] = user_ratings ['tmdbId'].astype('int')
    user_ratings['tmdbId'] = user_ratings['tmdbId'].astype(str)
    # Df_movie_metadata['id'] = Df_movie_metadata['id'].astype('int')
    user_metadata = pd.merge(Df_movie_metadata, user_ratings, left_on='id', right_on='tmdbId')
    user_metadata.drop(['tmdbId', 'userId'], axis=1, inplace=True)
    return user_metadata


def extract_names(json_string,colonneVoulu):
    if pd.isna(json_string):
        return []
    try:
        # Tenter de charger la chaîne JSON en un objet Python
        data = json.loads(json_string.replace("'",'"'))
    except json.JSONDecodeError:
        # En cas d'erreur, retourner une liste vide
        return []
    # Si la valeur est une liste de dictionnaires, extraire les valeurs pour la colonne spécifiée
    if isinstance(data, list):
        return [d.get(colonneVoulu, "") for d in data]
    # Si la valeur est un dictionnaire, extraire la valeur pour la colonne spécifiée
    if isinstance(data, dict):
        return [data.get(colonneVoulu, "")]
    # Si la valeur n'est ni une liste ni un dictionnaire, retourner une liste vide
    return []

def extractForColumn(base,colonne,colonneVoulu):
    base[colonne] = base[colonne].fillna('')
    base[colonne] = base[colonne].apply(lambda x: extract_names(x,colonneVoulu))

def liste_ID_genre(liste_genre, Df_movie_metadata=None):
    if Df_movie_metadata is None:
        Df_movie_metadata = pd.read_csv(CheminUnFichier('the-movie/movies_meta'), low_memory=False)
        
    # Créer une liste pour stocker les genres et leurs ids
    genres = []
    
    # Parcourir chaque ligne du dataframe
    for index, row in Df_movie_metadata.iterrows():
        # Extraire les genres de la ligne
        genre_list = eval(row['genres'])
        # Parcourir les genres de la ligne
        for genre in genre_list:
            # Vérifier si le genre est dans la liste recherchée
            if genre['name'].lower() in [g.lower() for g in liste_genre]:
                # Ajouter l'id et le nom du genre à la liste des résultats, si ce n'est pas déjà présent
                if (genre['id'], genre['name']) not in genres:
                    genres.append((genre['id'], genre['name']))
    return genres

def transforme_format_df(genres):
    resultat_dict = []
    for genre in genres:
        resultat_dict.append({"id": genre[0], "name": genre[1]})
    return resultat_dict

# print(transforme_format_df(liste_ID_genre(["drama", "comedy", "action"])))
