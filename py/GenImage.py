#Actualise les images sur la page principales
#ce fichier est appeler dans la cronTab sur le serveur 1 fois par jour
#On fais comme ça car il s'execute en 2 à 5 minutes en fonction de la machine


from AnalyseData import *

# Protect execution
if __name__=='__main__':
    
    location = "../archive/the-movies-dataset/"

    links_small = pd.read_csv(location+"links_small.csv")
    #Contains IMDB and TMDB IDs of all movies featured in the ratings_small.csv file (About 9000 movies).

    movies_metadata = pd.read_csv(location+"movies_metadata.csv", low_memory=False)
    #Contains adult, belongs_to_collection, budget, genres, homepage, id, imdb_id, original_language, original_title, overview
    # popularity, poster_path, production_companies, production_countries, release_date, revenue, runtime, spoken_languages, status
    # tagline, title, video, vote_average, vote_count
    
    ratings_small = pd.read_csv(location+"ratings_small.csv")
    #Contains userId, movieId, rating, timestamp
    
    movies_metadata = clean(movies_metadata)
    links_small = cleanAnnalyse(links_small, ratings_small) 
    
    q=3
    matKnn = MatrixReco(movies_metadata, links_small, ratings_small, algo_='KNN')
    matKnn = imputerData(matKnn, 'most_frequent')
    KMeansKnn = methodeKMeans(matKnn, q)
    ACP(matKnn, 0.9, annotation=False, cluster=KMeansKnn, nom="KMeans_prédiction_Knn")

    matSvd = MatrixReco(movies_metadata, links_small, ratings_small, algo_='SVD')
    KMeansSvd = methodeKMeans(matSvd, q)
    ACP(matSvd, 0.9, annotation=False, cluster=KMeansSvd, nom="KMeans_prédiction_Svd")
    AgglomerativeComparaison(matSvd, q)
    deterCoudé(matSvd)
    hybride(matSvd, strat = 'average', Qkmeans1=16, Qkmeans2=6)


