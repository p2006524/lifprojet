import pandas as pd
from sklearn.preprocessing import MultiLabelBinarizer
import spacy
import spacy_cleaner
from spacy_cleaner.processing import removers, replacers, mutators
import concurrent.futures
import threading


labels = [['a b c'],['a b d'],['a b c']]
mlb = MultiLabelBinarizer()
binary_labels = mlb.fit_transform(labels)

model = spacy.load("en_core_web_sm")
pipeline = spacy_cleaner.Pipeline(
    model,
    removers.remove_stopword_token,
    replacers.replace_punctuation_token,
    mutators.mutate_lemma_token,
)

#print(binary_labels)

"""
moviesId,genres,
 """

#fonction pour predire quel film pourrais aimer un utilisateur
def prediction() :
    #recupere les bdd
    movies = pd.read_csv("../archive/the-movies-dataset/movies_metadata.csv")
    ratings = pd.read_csv("../archive/the-movies-dataset/ratings_small.csv")


    tabRec = list()
    userId = 0
    #boucle sur la bdd ratings par index
    for i in ratings.index :
        if(i<100) :
            #si l'user est different de la ligne precedente
            if(userId != ratings['userId'][i]) :
                #si
                if(1 <= len(tabRec) and 1 < len(tabRec[-1])) :
                    tabRec[-1][1] = trouverGenresVus(movies, tabRec[-1][0])
                tabRec.append([])
                tabRec[-1].append('')
                tabRec[-1].append('')
                userId = ratings['userId'][i]
            tabRec[-1][0] = tabRec[-1][0]+str(ratings['movieId'][i])+' '
    if(1 <= len(tabRec) and 1 < len(tabRec[-1])) :
        tabRec[-1][1] = trouverGenresVus(movies, tabRec[-1][0])
    print(tabRec)

def trouverGenresVus(movies, idFilmsVus) :
    return 'e'
#prediction()

#fonction pour retrouver tous les genres d'un film dans la base de film donnée
def genresVal(val) :
    val = val.split("'name': '") # on split la chaine pour retrouver tous les genres
    del val[0]
    genre = ""
    genres = []
    for i in val : #parcourir le tableau de noms de genres
        for j in i :
            if j == "'":
                break
            genre += j
        """ print(genre) """
        genre = genre.replace(' ', '-')
        genres.append(genre) #on ajoute je genre trouve au tableau de genres
        genre = ""
    return genres



def process_index(dbM, dbG, i, lock):
    with lock:
        genres = genresVal(dbM['genres'][i])
        print(i)
        if(isinstance(dbM['overview'][i], str)):
            description = pipeline.clean([dbM['overview'][i]])
            description = remplace(description[0], ['_IS_PUNCT_'])
            splDes = description.split()
            for g in genres: #pour chaque genres de l'index actuel
                genre = g
                for i in splDes:
                    dbG = genreAjouter(i, genre, dbG)
                    dbG.to_csv("../archive/databaseLoL/genres.csv", index=False)
    return dbG



""" with concurrent.futures.ThreadPoolExecutor() as executor:
    futures = [executor.submit(process_index, dbM, dbG, i) for i in dbM.index if i <= 100]
    for future in concurrent.futures.as_completed(futures):
        dbG = future.result() """
def mainTrouverGenre():
    genreCreer()
    """ lock = threading.Lock() """
    dbM = pd.read_csv("../archive/the-movies-dataset/movies_metadata.csv") #base de films
    dbG = pd.read_csv("../archive/databaseLoL/genres.csv")
    dbM_dict = dbM.to_dict('index')
    dbG_dict = dbG.to_dict('index')
    print(dbG_dict)
    for index,i in dbM_dict.items(): #parcours de tous les index de movie_metadata
        if(index > 100):
            break
        """ print(i['genres']) """
        genres = genresVal(i['genres'])
        if(isinstance(i['overview'], str)):
            description = pipeline.clean([i['overview']])
            splDes = remplace(description[0], ['_IS_PUNCT_']).split()
            for g in genres: #pour chaque genres de l'index actuel
                genre = g
                for mot in splDes:
                    """ dbG = genreAjouter(mot, genre, dbG) """
                    dbG_dict = genreAjouter2(mot, genre, dbG_dict)
    dbG = pd.DataFrame.from_dict(dbG_dict, orient='index')
    dbG.to_csv("../archive/databaseLoL/genres.csv", index=False)

def genreTrouver(description):
    db = pd.read_csv("../archive/databaseLoL/genres.csv")
    description = pipeline.clean([description])
    description = remplace(description[0], ['_IS_PUNCT_'])
    splDes = description.split()
    g = {}
    g['unknow'] = 0
    for i in splDes: #Parcourir la liste des mots de la description
        genres = genreChercherMot(i,db)
        """ print(i) """
        if genres is not None:
            splGenres = genres['genres'].split(" ")
            splOccurences = str(genres['occurences']).split(" ")
            indice = 0
            for j in splGenres:
                """ print(j) """
                if j in g:
                    g[j] += int(splOccurences[indice])
                else:
                    g[j] = int(splOccurences[indice])
                indice += 1
        else:
            g['unknow'] += 1
        """ print(g) """
    g = dict(sorted(g.items(), key=lambda t:t[1], reverse=True))
    gKeys = list(g.keys())
    #print(gKeys[0])
    key_list = []
    key_list.append(gKeys[0])
    if gKeys[1] in gKeys:
        #print(gKeys[1])
        key_list.append(gKeys[1])
    return key_list

def genreAmeliorer(description, genre):
    db = pd.read_csv("../archive/databaseLoL/genres.csv")
    dbG_dict = db.to_dict('index')
    desc = pipeline.clean([description])
    desc = remplace(desc[0], ['_IS_PUNCT_'])
    splDes = desc.split()
    key = genreTrouver(description)
    while(key[0] != genre):
        for d in splDes:
            dbG_dict = genreAjouter2(d,genre,dbG_dict)
        dbG = pd.DataFrame.from_dict(dbG_dict, orient='index')
        dbG.to_csv("../archive/databaseLoL/genres.csv", index=False)
        key = genreTrouver(description)


def genreChercherMot(mot, dataBase):
    index = 0
    for i in dataBase["mot"]:
        if i == mot:
            l = dataBase.iloc[index]
            return dataBase.iloc[index]
        index += 1
    return None

def genreCreer():
    genre = pd.DataFrame(columns=['mot', 'genres','occurences'])
    ajouter = pd.DataFrame([('','','')], columns=['mot', 'genres','occurences'])
    genre = pd.concat([genre,ajouter], ignore_index=True)
    genre.to_csv("../archive/databaseLoL/genres.csv", index=False)

def genreModifier():
    genre = pd.read_csv("../archive/databaseLoL/genres.csv")
    ajouter = pd.DataFrame([(None,"marrant")], columns=['horreur', 'comedie'])
    genre = genre.append(ajouter,ignore_index=False)
    genre.to_csv("../archive/databaseLoL/genres.csv")
    genre = pd.read_csv("../archive/databaseLoL/genres.csv")
    print(pd.isna(genre.iloc[0]['horreur']))

def genreAjouter(mot, genre, dataBase): #ajouter un mot avec son genre associé a la base
    index = 0
    trouve = False
    for i in dataBase["mot"]: #parcourir les mots dans la bd genres
        if mot in i: #si le mot correspond a celui dans genres
            trouve = True
            break
        index += 1
    if trouve: #si mot existe deja dans genres
        """ print("je suis la 1") """
        if(genre in dataBase.iloc[index]['genres']): #si le mot est deja associe a ce genre
            """ print("je suis la 1.1 "+mot) """
            splG = dataBase.iloc[index]['genres'].split(" ")
            genre = genre.split()
            splG = splG.index(genre[0])
            splO = str(dataBase.iloc[index]['occurences']).split(" ")
            splO[splG] = int(splO[splG])+1
            Occ = ""
            if len(splO) > 1:
                for i in splO:
                    Occ += str(i)+" "
                if Occ[-1] == " ":
                    Occ = Occ[:-1]

            else:
                Occ = splO[0]
            dataBase.loc[dataBase.index[index],'occurences'] = Occ
            """ print(dataBase.iloc[index]['occurences']) """
        else: #si le mot n'est pas associe a ce genre
            """ print("je suis la 1.2") """
            dataBase.loc[dataBase.index[index],'genres'] += " "+genre
            dataBase.loc[dataBase.index[index],'occurences'] = str(dataBase.iloc[index]['occurences'])+" 1"
    else: #si aucun mot trouve
        """ print("je suis la 2 "+mot) """
        ajouter = pd.DataFrame([(mot,genre,'1')], columns=['mot', 'genres','occurences'])
        dataBase = pd.concat([dataBase,ajouter], ignore_index=True)
    """ dataBase.to_csv("../archive/databaseLoL/genres.csv", index=False) """
    return dataBase

def genreAjouter2(mot, genre, dataBase): #ajouter un mot avec son genre associé a la base
    index = 0
    trouve = False
    for cle in dataBase.keys(): #parcourir les mots dans la bd genres
        if mot in str(dataBase[cle]['mot']) and int(cle) != 0: #si le mot correspond a celui dans genres
            trouve = True
            index = cle
            break
    if trouve: #si mot existe deja dans genres
        if(genre in dataBase[index]['genres']): #si le mot est deja associe a ce genre
            splG = dataBase[index]['genres'].split(" ")
            genre = genre.split()
            splG = splG.index(genre[0])
            splO = str(dataBase[index]['occurences']).split(" ")
            splO[splG] = int(splO[splG])+1
            Occ = ""
            if len(splO) > 1:
                for i in splO:
                    Occ += str(i)+" "
                if Occ[-1] == " ":
                    Occ = Occ[:-1]

            else:
                Occ = splO[0]
            dataBase[index]['occurences'] = Occ
        else: #si le mot n'est pas associe a ce genre
            dataBase[index]['genres'] += " "+genre
            dataBase[index]['occurences'] = str(dataBase[index]['occurences'])+" 1"
    else: #si aucun mot trouve
        index = list(dataBase.keys())[-1]
        index = int(index) + 1
        dataBase[index] = {'mot': mot, 'genres': genre, 'occurences': '1'}
    return dataBase

def remplace(string, rep):
    for i in rep:
        string = string.replace(i, ' ')
    return string



def filtrer(base, stat, inf, sup):
    return base[(base[stat] > inf) & (base[stat] < sup)]

def trier(base, stat, ordre):
    if ordre == ">":
        return base.sort_values(by=stat)
    elif ordre == "<":
        return base.sort_values(by=stat, ascending=False)

def classer(base, stat):
    classement = {}
    for i in base[stat]:
        if i in classement:
            classement[i] += 1
        else:
            classement[i] = 1
    return sorted(classement.items(), key=lambda t:t[1])

def grouper(base, stat):
    b = trier(base, stat[0], ">")
    groupe = {}
    col = []
    for i in b:
        col.append(i)
    for i in range(len(b)):
        groupeAct = groupe
        for j in stat:
            indice = b.loc[i,j]
            if(not(indice in groupeAct)):
                groupeAct[indice] = {}
            groupeAct = groupeAct[indice]

        for k in col:
            if(not(k in groupeAct)):
                groupeAct[k] = []
            groupeAct[k].append(b.loc[i,k])
    return groupe

# Protect execution
if __name__=='__main__':
    """ prediction() """
    """ mainTrouverGenre() """
    """ genreAmeliorer("Led by Woody, Andy's toys live happily in his room until Andy's birthday brings Buzz Lightyear onto the scene. Afraid of losing his place in Andy's heart, Woody plots against Buzz. But when circumstances separate Buzz and Woody from their owner, the duo eventually learns to put aside their differences.", "Animation") """
    genreTrouver("Led by Woody, Andy's toys live happily in his room until Andy's birthday brings Buzz Lightyear onto the scene. Afraid of losing his place in Andy's heart, Woody plots against Buzz. But when circumstances separate Buzz and Woody from their owner, the duo eventually learns to put aside their differences.")

    """ genreTrouver("When siblings Judy and Peter discover an enchanted board game that opens the door to a magical world, they unwittingly invite Alan -- an adult who's been trapped inside the game for 26 years -- into their living room. Alan's only hope for freedom is to finish the game, which proves risky as all three find themselves running from giant rhinoceroses, evil monkeys and other terrifying creatures.") """

    """ genreCreer() """
    """ genreFilm("terrifiant, drole! mais marrant.", "") """

""" df = jeanbapt()

m2 = filtrer(jeanbapt(), "stats.attackspeed", 0.650, 0.652)
m3 = trier(jeanbapt(), "stats.attackspeed", "<")
m4 = classer(jeanbapt(), "stats.attackspeed")
m4.reverse()
m5 = grouper(jeanbapt(), ["stats.attackspeed","stats.attackspeedperlevel"])
print(list(m2["name"]))
print(list(m3["stats.attackspeed"]))
print(m4)

print(m5) """
