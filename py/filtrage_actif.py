# Ce fichier utilise la bibliothèque surprise pour utiliser des algorithmes de recomandation 
# Le lien de la documentation : https://surpriselib.com/
# 
# Ce fichier utilise la bibliothèque sklearn
# Le lien de la documentation : https://scikit-learn.org/stable/



from system import *
from recherche_dans_bd import *

import dataFrame as df
import numpy as np
import pandas as pd
from surprise import Reader, Dataset, accuracy, KNNBasic,SVDpp
from surprise.model_selection import train_test_split
from surprise.prediction_algorithms import SVD 
from surprise.model_selection import cross_validate
# from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.impute import SimpleImputer 
from sklearn.metrics.pairwise import cosine_similarity
import multiprocessing
import threading

import contextlib
# from tqdm import tqdm

def adapte_Data_Pour_Algo(ratings_df):
    """
    fonction interne a ne pas utiiser en externe
    permet d'addapter la df ratings_df pour les algos de prédiction
    return la df du bon "format"
    """
    ratings_df = ratings_df.iloc[:, :3]
    ratings_df = ratings_df.rename(columns={'user': 'userId', 'item': 'movieId', 'rating': 'rating'}) # on renome pour être compatible avec les fonction de soup
    reader = Reader(rating_scale=(1, 5)) # Definir l'échelle de rating
    return Dataset.load_from_df(ratings_df, reader) # Chargement des données dans le dataset


def entrainement_Model(ratings_df = None, model = SVD):
    """
    permet d'entrainner un algo de prediction,
    fonctionne avec au moins SVD et KNNbasic
    return l'ago qui est utiliser pour prédire une note
    """
    if ratings_df is None:
        ratings_df = pd.read_csv(CheminUnFichier('the-movie/ratings_sm'))
    data = adapte_Data_Pour_Algo(ratings_df)
    algo = model()
    with open('log.txt', 'w') as f:
        with contextlib.redirect_stdout(f):
            algo.fit(data.build_full_trainset())
    return algo



def evaluation_Model(ratings_df = None, model = SVD):
    """
    permet d'évaluer la performance d'un algo de prediction,
    compatible avec les algos de surprices
    retourne le rmse soit une note entre 0 et 1 qui détermine la présision de l'ago
    """
    if ratings_df is None :
        ratings_df = pd.read_csv(CheminUnFichier('the-movie/ratings_sm'))
    # charger les données dans un dataset surprise
    data = adapte_Data_Pour_Algo(ratings_df)
    # diviser le dataset en un ensemble de formation et un ensemble de test
    trainset, testset = train_test_split(data, test_size=0.25)
    #print("créer un modèle et l'entraîne sur l'ensemble de formation")
    model = model()
    model.fit(trainset)
    #print(" faire des prédictions sur l'ensemble de test ")
    predictions = model.test(testset)
    # calculer la RMSE sur l'ensemble de test
    rmse = accuracy.rmse(predictions)
    return rmse


def predire_Note_Utilisateur(titre_Film, num_Utili, movie_metadata_df = None,ratings_df = None, links_df = None, algo = None):
    """
    permet de predire une note pour un film, il faut soit l'id correspondant a l'id de links ou ratings
    /!\ pas l'id de movies_metadata
    retourne une liste d'information sur le film, dans ce format :
    user:(l'utilisateur)  item:(l'id du film)  r_ui = None   est = (la note predite)   {'was_impossible': (booléen qui est faux si on a pu prédire)}
    """
    if ratings_df is None:
        #print("pas de df renseigner on prend movies01/ratings_sm")
        ratings_df = pd.read_csv(CheminUnFichier('movies01/ratings_sm'))
    if algo is None:
        #print("algo de prediction pas renseignier on va le créé avec SVD")
        algo = entrainement_Model(ratings_df)
    if movie_metadata_df is None:
        movie_metadata_df = pd.read_csv(CheminUnFichier('movies01/movies_meta'), low_memory=False)
    if type(titre_Film) == str:
        links_df = pd.read_csv(CheminUnFichier('movies01/links.csv'))
        id_Movie = id_links_Par_Titre(nom_Du_Film = titre_Film, Df_links = links_df, Df_title = movie_metadata_df)
    else:
        id_Movie = titre_Film
    prediction = algo.predict(num_Utili, id_Movie)
    return prediction


def meilleur_Film(num_Utili, algo, data = None, ratings_df = None, links_df = None, sort = True):
    '''
        permet de claculer les notes prédite de chaque film pour un utilisateur.
    '''
    if data is None:
        data = pd.read_csv(CheminUnFichier('the-movies/movies_metadata'), low_memory=False)
    if ratings_df is None:
        ratings_df = pd.read_csv(CheminUnFichier('the-movies/ratings_sm'))
    if links_df is None:
        links_df = pd.read_csv(CheminUnFichier('the-movies/links_sm'))
    if links_df is None:
        links_df = pd.read_csv(CheminUnFichier('the-movies/links_sm'))
    liste_note = []
    liste_non_precis = []
    liste = []
    for i in links_df['movieId']:
        liste.append(predire_Note_Utilisateur(i, num_Utili, data, ratings_df, links_df, algo))
    for i in liste:
        if i[4] ['was_impossible']:
            liste_non_precis.append((i.est, i.iid))
        else:
            liste_note.append((i.est, i.iid))
    if sort:
        liste_non_precis.sort(key=lambda x: x[0], reverse=True) 
        liste_note.sort(key=lambda x: x[0], reverse=True)
    retour = [None] * 2
    retour [0] = liste_note
    retour [1] = liste_non_precis
    return retour


def entrainne_KNNBasic(ratings_df = None):
    '''
        permet d'entrainner l'aglo KNNBasic
        ratings_df = est les données qui permettent d'entrainner l'aglo, pour nos films c'est les votes des utilisateurs
        on retourne l'aglo
    '''
    if ratings_df is None:
        ratings_df = pd.read_csv(CheminUnFichier('movies/ratings_sm'))
    return entrainement_Model(ratings_df, model=KNNBasic)

def entrainne_SVD(ratings_df = None):
    '''
        permet d'entrainner l'aglo SVD
        ratings_df = est les données qui permettent d'entrainner l'aglo, pour nos films c'est les votes des utilisateurs
        on retourne l'aglo
    '''
    if ratings_df is None:
        ratings_df = pd.read_csv(CheminUnFichier('the-movie/ratings_sm'))
    return entrainement_Model(ratings_df, model=SVD)
    
def meilleur_Film2(num_Utili, algo, data=None, ratings_df=None, links_df=None, sort = True):
    '''
        version pour adapter aux fonction de Valentin
    '''
    if links_df is None:
        links_df = pd.read_csv(CheminUnFichier('movies01/links_sm'))
    #print("c'est lu")
    liste_note = []
    liste_non_precis = []
    # join = join_Two_File(links_df, data)

    liste = []
    for i in links_df['movieId']:     #links_df et rating n'ont pas le même nombre de film
        liste.append(predire_Note_Utilisateur(i, num_Utili, data, ratings_df, links_df, algo))
    for i in liste:
        if i[4] ['was_impossible']:
            liste_non_precis.append((i.est, i.iid))
            liste_note.append(np.nan)
            if sort:
                liste_non_precis.sort(key=lambda x: x[0], reverse=True) 
        else:
            liste_note.append(i.est)
            if sort:
                liste_note.sort(key=lambda x: x[0], reverse=True)
    retour = [None] * 2
    retour [0] = liste_note
    retour [1] = liste_non_precis
    return retour        

def MatrixReco(movies_metadata, links_small, ratings_small, algo_='SVD'):
    '''
        cree une matrice utilisateurs/flim avec leur prédiction,
        utilise l'algo SVD ou KNN
    '''
    if algo_ == 'KNN' :
        ALGO = entrainne_KNNBasic(ratings_small)
    elif algo_ == 'SVD' : 
        ALGO = entrainne_SVD(ratings_small)
    userList = ratings_small['userId'].drop_duplicates()
    filmList = links_small['movieId'].drop_duplicates()
    matrix = pd.DataFrame(index=userList, columns=filmList)
    for i in userList:
        useri = meilleur_Film2(i, data=movies_metadata, ratings_df=ratings_small, links_df=links_small, algo = ALGO, sort = False)
        matrix.loc[i] = useri[0]
    matrix.to_csv('../archive/the-movies-dataset/predUser_Movie.csv')
    return matrix

def RecoParSim(Ui, Movie, matNote, matNote_imputed, matSim):
    '''
        recomandation pour un fim et un utilisateur avec l'aglo de similarité
        retourne la note prédite
    '''
    Ui_vecS = []
    for i in range(len(matNote_imputed)):
        if i+1 == Ui:
            Ui_vecS.append(0)
        elif matNote[Movie][i+1] != np.nan:
            Ui_vecS.append(matSim[Ui-1][i])
            #print(matSim[Ui][i])
        else:
            Ui_vecS.append(0)
    denom = sum(Ui_vecS)
    Mov_vec = np.array(matNote_imputed[Movie])
    num = np.dot(Ui_vecS, Mov_vec)
    return (num/denom, Movie)

def RecoSimUi(Ui, links_small, matNote, matNote_imputed, matSim, thread_id, result_List=None):
    '''
        permet de calculer pour un utilisateur toutes ses recomandations des films présent dans links_small
    '''
    listeNote = []
    for i in links_small['movieId']:
        #print(i)
        listeNote.append(RecoParSim(Ui, i, matNote, matNote_imputed, matSim))
    if result_List != None: 
        result_List [thread_id] = listeNote
    return listeNote

def RecoSimUiThread(Ui, ratings_small, links_small,  matNote, matNote_imputed, matSim, num_threads = 4):
    '''
        permet d'utiliser des threads pour appliquer l'ago de recomandation par similarité 
    '''
    num_films = len(links_small)
    films_per_thread = (num_films + num_threads - 1) // num_threads  # arrondi supérieur le nombre de films par threads
    film_lists = [links_small[ i:i + films_per_thread] for i in range(0, num_films, films_per_thread)] # les listes contenant films_per_thread flims

    # Lancer un thread pour chaque sous-liste de films
    threads = []
    result_list = [[] for _ in range(len(film_lists))]
    for i, films in enumerate(film_lists):
        thread = threading.Thread(target = RecoSimUi, args = (Ui, films, matNote, matNote_imputed, matSim, i, result_list))
        threads.append(thread)
        thread.start()
    result_Final = []
    for i, thread in enumerate(threads):
        thread.join()
        result_Final += result_list[i]
    return result_Final

def imputerData(base, strat, fill_value=None):
    '''
        permet de remplacer les NAN par la moyenne ou la strategie fournit
    '''
    imputer = SimpleImputer(strategy=strat, fill_value=fill_value)
    return pd.DataFrame(index=base.index, columns=base.columns, data=imputer.fit_transform(base))

def RecoSimFinal(Ui,movies_metadata, links_small, ratings_small, imputedStrat = 'most_frequent', sort = True, nbThread=4):
    '''
        Essaie de créer une matrice utilisateurs/flim avec leur prédiction,
        utilise l'algo Sim mais c'est trop long, à faire tourner environ 6 heures pour être bon
    '''
    userList = ratings_small['userId'].drop_duplicates()
    filmList = links_small['movieId'].drop_duplicates()
    matNote = ratings_small.pivot(index='userId', columns='movieId', values='rating')
    matNote_imputed = imputerData(matNote,imputedStrat)
    matSim = cosine_similarity(matNote_imputed.to_numpy())
    useri = RecoSimUiThread(Ui, ratings_small, links_small, matNote, matNote_imputed, matSim, nbThread)
    if sort:
        useri.sort(reverse=True)
    #print( "utilisateur",Ui,"traité \n")
    return useri