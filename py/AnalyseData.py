# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # J'aime bien repartir de 0

# Nouveau fichier que Arthur  et moi utiliserons pour implémenter mes fonctions que je vais reprendre de FiltrageCollaboratif.ipynb

# +
import pandas as pd
import numpy as np
import networkx as nx
import statistics as stats
import warnings
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs
from sklearn.decomposition import PCA as pc
from sklearn.preprocessing import StandardScaler
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn import metrics
from sklearn.cluster import AgglomerativeClustering
from sklearn.impute import SimpleImputer
import matplotlib.pyplot as plt
import matplotlib

from filtrage_actif import *
from dataFrame import *
# -


# Protect execution
if __name__=='__main__':
    
    location = "../archive/the-movies-dataset/"

    links_small = pd.read_csv(location+"links_small.csv")
    #Contains IMDB and TMDB IDs of all movies featured in the ratings_small.csv file (About 9000 movies).

    movies_metadata = pd.read_csv(location+"movies_metadata.csv", low_memory=False)
    #Contains adult, belongs_to_collection, budget, genres, homepage, id, imdb_id, original_language, original_title, overview
    # popularity, poster_path, production_companies, production_countries, release_date, revenue, runtime, spoken_languages, status
    # tagline, title, video, vote_average, vote_count
    
    ratings_small = pd.read_csv(location+"ratings_small.csv")
    #Contains userId, movieId, rating, timestamp
    
    movies_metadata = clean(movies_metadata)
    links_small = cleanAnnalyse(links_small, ratings_small)
#j'ai eu du mal à règler ce warning,
#vu qu'il avertie que ça fonctionnera pas dans une futures version vu que l'on utilise conda et que l'on force une version de python, on s'en fout 

# # Recommendation

# +
#les strat possibles sont: mean, median, most_frequent, constant(avec fill_value)
def imputerData(base, strat, fill_value=None):
    '''
        permet de remplacer les NAN par la moyenne ou la strategie fournit
    '''
    imputer = SimpleImputer(strategy=strat, fill_value=fill_value)
    return pd.DataFrame(index=base.index, columns=base.columns, data=imputer.fit_transform(base))

def statSurListe(liste_valeurs):
    '''
        calcul les statistiques en fonction de liste_valeurs
        utiliser dans TestEfficacite
    '''
    moyenne = stats.mean(liste_valeurs)
    mediane = stats.median(liste_valeurs)
    ecart_type = stats.stdev(liste_valeurs)
    variance = stats.variance(liste_valeurs)
    min_valeur = min(liste_valeurs)
    max_valeur = max(liste_valeurs)
    print("Moyenne : ", moyenne)
    print("Médiane : ", mediane)
    print("Écart-type : ", ecart_type)
    print("Variance : ", variance)
    print("Valeur minimale : ", min_valeur)
    print("Valeur maximale : ", max_valeur)
    return [moyenne,mediane,ecart_type,variance,min_valeur,max_valeur]


def TestEfficacite(matRec, ratings_small):
    '''
        teste l'efficasiter d'une matrice de recomandation,
        la matrice est dans se style
            film
        user
        retourne les différence entre les vrai notes et les prédictions
        attention met Beaucoup de temps à executer que rarement 
    '''
    vec = list()
    matNote=ratings_small.pivot(index='userId', columns='movieId', values='rating')
    for j in matRec.columns:
        for i in range(len(matRec)):
            if matNote[j][i+1] != np.nan:
                vec.append(np.abs(matNote[j][i+1]-matRec[j][i+1]))
    vec = [x for x in vec if not np.isnan(x)]
    return statSurListe(vec)

#déplacé dans Filtrage_actif:

#def RecoParSim(Ui, Movie, matNote, matNote_imputed, matSim)
#def statSurListe(liste_valeurs):
#def RecoSimUi(Ui, links_small, matNote, matNote_imputed, matSim, thread_id, result_List):
#def RecoSimUiThread(Ui, ratings_small, links_small,  matNote, matNote_imputed, matSim, num_threads = 4):
#def MatrixReco(movies_metadata, links_small, ratings_small,algorithm='KNN'):
#def MatrixRecoSim(movies_metadata, links_small, ratings_small, imputedStrat = 'mean'):
# -

# Protect execution
#if __name__=='__main__':
    #mat = MatrixReco(movies_metadata, links_small, ratings_small,algorithm='SVD')
    #mat =  pd.read_csv('../archive/the-movies-dataset/predUser_MovieSVD.csv', index_col=0)
    #mat.columns = np.int64(mat.columns[:].values)
    #mat_imput = imputerData(mat, 'most_frequent')
    #TestEfficacite(mat_imput,ratings_small)


# ### pour-l'algo-KNN-l'erreur:
# Moyenne : 0.5508478323336021 mediane : 0.43651757565542737 Ecart-type : 0.48162064502396457 Variance : 0.23195844571329968 Valeur minimale : 0.0 Valeur maximale : 3.5953803517466634
#
# ### pour l'algo SVD l'erreur: 
# Moyenne :  0.4981030737659244, Médiane :  0.40348317880580353, Écart-type :  0.4081490604716553, Variance :  0.1665856555638949, Valeur minimale :  0.0, Valeur maximale :  3.5233605808069486
#
# ### Conclusion claire: 
# SVD est plus efficace que KNN, les erreurs sont plus minim et donc l'analyse de donnée aura plus de sens sur celui çi

def CentreReduire(data):
    '''
    Permet de centrer réduire les données, important pour effacer l'échelle
    https://fr.wikipedia.org/wiki/Variable_centr%C3%A9e_r%C3%A9duite
    '''
    scaler = StandardScaler()
    Xnorm = scaler.fit(data)
    Xnorm  = scaler.transform(data)
    return Xnorm


def ACP(data, minPerte, annotation=True, cluster=[], nom="") :
    '''
    Permet de visualiser les donner sur 2 axes
    indique la perte d'info 
    '''
    labels = data.index
    Xnorm = CentreReduire(data)
    
    sklearn_pca = pc(n_components=len(Xnorm)) 
    sklearn_transf = sklearn_pca.fit_transform(Xnorm)
    pourcentage = sklearn_pca.explained_variance_ratio_
    res = 0
    i=0
    res2vect = 0
    while res < minPerte:
        res += pourcentage[i]
        i += 1
        if(i == 2):
            res2vect = res
    print("pour conserver plus de ",minPerte,"% d'information il faut conserver ",i," vecteurs")
    if(i != 2):
        print("on conserve ",res2vect,"% d'information pour les 2 vecteurs du graphique")
        
    colors = ['red','yellow','blue','pink','k','m','g','c']
    plt.figure(figsize=(14, 10))
    sklearn_pca = pc(n_components=i)
    sklearn_transf = sklearn_pca.fit_transform(Xnorm)
    if np.any(cluster):
        print("efficacité du clustering: ",metrics.silhouette_score(Xnorm,cluster.labels_))
        plt.scatter(sklearn_transf[:, 0], sklearn_transf[:, 1], c= cluster.labels_, 
                    cmap=matplotlib.colors.ListedColormap(colors))
    else:
        plt.scatter(sklearn_transf[:, 0], sklearn_transf[:, 1])
    if(annotation):
        for l, x, y in zip(labels, sklearn_transf[:, 0], sklearn_transf[:, 1]):
            plt.annotate(l, xy=(x, y), xytext=(-0.2, 0.2), textcoords='offset points')
    plt.title("ACP qui conserve {} % d'info pour 2 vecteurs du graphique".format(res2vect))
    plt.suptitle("methode utilisé: "+nom, y=0)
    plt.xticks([])
    plt.yticks([])
    locSave = "../img/ACP"+nom+".png"
    plt.savefig(locSave)
    return plt
#mean:0.21129526064112852, median:0.21127643844569893, most_frequent:0.2113218331776789, constant: 0.1830285345879156
#avec notre jeu de données, c'est l'imputation most_frequente qui est la plus efficace


# # Clustering

def methodeKMeans(X, q):
    '''
        la fameuse méthode Kmeans avec 30 itération pour trouver 
        des centres de début optimaux
    '''
    Xnorm = CentreReduire(X)
    model = KMeans(n_clusters=q, n_init=30).fit(Xnorm)
    return model


def methodeAgglomerativeClustering(data, strat='ward', q=2):
    '''
        el famoso CAH 
    '''
    Xnorm = CentreReduire(data)
    return AgglomerativeClustering(n_clusters=q, linkage=strat).fit(Xnorm)

# permet de trouver quelle est la meilleur méthode
def deterQ(data,nbC):
    '''
        Permet de trouver le nombre de cluster pour toutes les méthodes
        Attention très lent
    '''
    Xnorm = CentreReduire(data)
    matrix = pd.DataFrame(index=['kmeans','single','average','ward'], columns=range(2,nbC+1))
    SilhouetteK = []
    SilhouetteS = []
    SilhouetteA = []
    SilhouetteW = []
    for i in range(2,nbC+1):
        cluster = methodeKMeans(data, i)
        SilhouetteK.append(metrics.silhouette_score(Xnorm,cluster.labels_))
        cluster = methodeAgglomerativeClustering(data, 'single', i+2)
        SilhouetteS.append(metrics.silhouette_score(Xnorm,cluster.labels_))
        cluster = methodeAgglomerativeClustering(data, 'average', i+2)
        SilhouetteA.append(metrics.silhouette_score(Xnorm,cluster.labels_))
        cluster = methodeAgglomerativeClustering(data, 'ward', i+2)
        SilhouetteW.append(metrics.silhouette_score(Xnorm,cluster.labels_))
    matrix.loc['kmeans'] = SilhouetteK
    matrix.loc['single'] = SilhouetteS
    matrix.loc['average'] = SilhouetteA
    matrix.loc['ward'] = SilhouetteW
    return matrix


# +
#res = deterQ(mat_imput, 10)
#res
# -
def deterCoudé(data):
    '''
        utilisé montre qu'il faut utiliser les valeurs quand la courbe s'applanie
        il faut regarder au coude quoi, itération de Kmeans avec Q qui change
    '''
    Xnorm = CentreReduire(data)
    
    # Créer une liste pour stocker les valeurs de l'inertie
    inertias = []

    # Définir une plage de valeurs de k pour tester
    k_values = range(2, 20)

    # Boucle sur les différentes valeurs de k
    for k in k_values:
        # Instancier un modèle KMeans avec k clusters
        kmeans = KMeans(n_clusters=k, random_state=0,  n_init=10)
        kmeans.fit(Xnorm)
        # Ajouter l'inertie à la liste
        inertias.append(kmeans.inertia_)

    # Tracer la courbe de l'inertie par rapport à k
    plt.figure(figsize=(14, 10))
    plt.plot(k_values, inertias, '-o')
    plt.xlabel('Nombre de clusters (k)')
    plt.ylabel("Inertie")
    plt.title("Méthode du coude")
    plt.savefig("../img/deterCoude.png")
#deterQ2(mat_imput)


# On remarque que Kmeans est le plus efficace jusqu'a 6 cluster, après c'est average qui est plus efficace, on va faire une méthode hybride de average et Kmeans pour avoir plus de stable

def hybride(data, strat = 'average', Qkmeans1=16, Qkmeans2=5) :
    '''
        la fameuse méthode hybrique qui enlève les désavantage de Kmeans
    '''
    #KMeans
    Xnorm = CentreReduire(data)
    model = methodeKMeans(data, Qkmeans1)
    centre_KMeans = model.cluster_centers_

    #CAH
    clustering = AgglomerativeClustering(n_clusters=Qkmeans2, linkage=strat).fit(centre_KMeans)
    CAH_label = clustering.labels_
    clusters = []
    for i in range(Qkmeans2):
        clusters.append(np.where(CAH_label==i)[0])
    
    fig, axs = plt.subplots(1, 2, figsize=(18, 6), sharey=False)
    dendrogram(linkage(clustering.children_, strat))
    centres = []
    for i in range(Qkmeans2):
        centres.append(centre_KMeans[clusters[i]].mean(axis=0))

    #KMeans 2
    model = KMeans(n_clusters=Qkmeans2 , init=centres, n_init=1).fit(Xnorm)
    centre_KMeans = model.cluster_centers_

    pca=pc(n_components=2)
    pca.fit(Xnorm)
    X_pca=pca.transform(Xnorm)

    colors = ['red','yellow','blue','pink','k','m','g','c']
    scat = axs[0].scatter(X_pca[:, 0], X_pca[:, 1], c= model.labels_,
    cmap=matplotlib.colors.ListedColormap(colors[:Qkmeans2]))
    # Ajouter une légende qui spécifie les numéros de cluster de 1 à Qkmeans2
    legend_labels = [str(i+1) for i in range(Qkmeans2)]
    axs[0].legend(handles=[plt.scatter([],[], color=colors[i], label=label) for i, label in enumerate(legend_labels)],
                  title="Groupes", loc='upper left')
    print("efficacite hybride: ",metrics.silhouette_score(Xnorm, model.labels_))
    axs[0].set_xticks([])
    axs[0].set_yticks([])
    axs[0].set_title('ACP hybride, efficacité du clustering: {}'.format(metrics.silhouette_score(Xnorm, model.labels_)))
    axs[1].set_title('dendrogramme sur centre Kmeans')
    locSave = "../img/hybride_"+strat+".png"
    fig.savefig(locSave)
    return model


def AgglomerativeComparaison(data, q):
    '''
        un récap graphique des sous-méthodes de séléction de la CAH 
    '''
    fig, axs = plt.subplots(2, 3, figsize=(18, 6), sharey=False)
    single = methodeAgglomerativeClustering(data, 'single',q)
    dendrogram(linkage(single.children_, 'single'), ax=axs[0][0])
    
    average = methodeAgglomerativeClustering(data, 'average',q)
    dendrogram(linkage(average.children_, 'average'), ax=axs[0][1])

    ward = methodeAgglomerativeClustering(data, 'ward',q)
    dendrogram(linkage(ward.children_, 'ward'), ax=axs[0][2])
    
    Xnorm = CentreReduire(data)
    pca=pc(n_components=2)
    pca.fit(Xnorm)
    X_pca=pca.transform(Xnorm)

    colors = ['red','yellow','blue','pink','k','m','g','c']
    axs[1][0].scatter(X_pca[:, 0], X_pca[:, 1], c= single.labels_,
    cmap=matplotlib.colors.ListedColormap(colors))
    axs[1][1].scatter(X_pca[:, 0], X_pca[:, 1], c= average.labels_,
    cmap=matplotlib.colors.ListedColormap(colors))
    axs[1][2].scatter(X_pca[:, 0], X_pca[:, 1], c= ward.labels_,
    cmap=matplotlib.colors.ListedColormap(colors))
    axs[1][0].set_xticks([])
    axs[1][0].set_yticks([])
    axs[1][1].set_xticks([])
    axs[1][1].set_yticks([])
    axs[1][2].set_xticks([])
    axs[1][2].set_yticks([])
    
    axs[0][0].set_title("CAH par single:")
    axs[0][1].set_title("CAH par average:")
    axs[0][2].set_title("CAH par ward:")
    
    locSave = "../img/CAHcomparaison.png"
    fig.savefig(locSave)


# # Analyse des groupes

# +
def UsePartition(listPartition,q):
    '''
        donne un tableau de q groupes pour chaque utilisateur
    '''
    tabGrp = []
    for i in range(q):
        tabTmp = []
        for index, j in enumerate(listPartition):
            if j==i:
                tabTmp.append(index+1)
        tabGrp.append(tabTmp)
    return tabGrp

def PointCommun(Partition, movies_metadata, links_small, ratings_small):
    '''
        resort la listes des films évaluer par les utilisateur
    '''
    filtre0 = ratings_small[ratings_small['userId'].isin(Partition)]
    filtre1 = filtre0["movieId"]
    return infoFromId(filtre1, movies_metadata, links_small)


# -


def ClustersMovies(clusters, q, movies_metadata, links_small, ratings_small):
    '''
        permet de récup une sous-base de donnée pour chaque groupes
    '''
    tabGrp = UsePartition(clusters.labels_, q)
    BDInterpretable = []
    for i in range(q):
        BDInterpretable.append(PointCommun(tabGrp[i], movies_metadata, links_small,ratings_small))
    return BDInterpretable, tabGrp


def nbGroupe(tabgrp):
    '''
        donne la répartiton des utilisateurs dans les groupes
    '''
    calc = pd.Series(index=range(1,len(tabgrp)+1), dtype=float)
    calc2 = 0
    for i in tabgrp:
        calc2 += len(i)
    for i in calc.index:
        calc[i] = (len(tabgrp[i-1])/calc2)*100
    #plt.pie(calc, labels=calc.index, autopct='%1.1f%%')
    #plt.title("répartition des utilisateur dans les groupes")
    #plt.savefig("../img/repartionUi_Grp.png")
    return calc


# Comprendre son groupe:

def VotreGroupe(Ui, BDI, tabGrp):
    '''
        donne l'info de son groupe et de ses voisin
    '''
    leGroupe = -1
    for i, grp in enumerate(tabGrp):
        if(Ui in grp):
            leGroupe = i
    if(leGroupe == -1):
        print("erreur tu es dans aucun groupe")
        return
    calc = len(tabGrp[leGroupe])
    res = "vous etes dans le groupe "+str(leGroupe+1)+" qui est compose de "+str(calc)+" membres \n"
    pourcent = nbGroupe(tabGrp)
    res +="c'est a dire "+str(pourcent[leGroupe+1])+" % de tout les utilisateurs. \n"
    res +="voici les autres utilisateurs de votre groupe:\n"+str(tabGrp[leGroupe])+"\n"
    return leGroupe, res


# ## les genres

def SelectionGenres(BDI, ratings_small, GrpParGenres, axs, nom= 'BDI'):
    '''
        pour un groupe donne sa répartition des évaluations par films
    '''
    nbEval = GrpParGenres.count().drop('id')
    pourcent = pd.Series(index=nbEval.index, dtype=float)
    for i in nbEval.index:
        pourcent[i] = (nbEval[i]/sum(nbEval)) * 100
    nbEval = nbEval.sort_values(ascending=False)
    pourcent = pourcent.sort_values(ascending=False)
    axs.pie(pourcent, labels=nbEval.index, autopct='%1.1f%%')
    axs.set_title("répartion des genres des films evalué du groupe {}".format(nom))
    return pourcent


def CompareSelectGenres(GRP, GRP2, ratings_small, nom1='BDI', nom2='BDI2'):
    '''
        Comparaison de 2 SelectionGenres
    '''
    GRP_DfGenres = GroupeParGenres(GRP)    #df qui met  dans les collums genres    
    GRP2_DfGenres = GroupeParGenres(GRP2)    #df qui met  dans les collums films
    fig, axs = plt.subplots(1, 2, figsize=(18, 6), sharey=False)
    prcEval = SelectionGenres(GRP, ratings_small, GRP_DfGenres, axs[0], nom1)
    prcEval2 = SelectionGenres(GRP2, ratings_small, GRP2_DfGenres, axs[1], nom2)
    locSave = "../img/CompareSelectGenre.png"
    fig.savefig(locSave)
    
    res = prcEval-prcEval2
    res = res.sort_values(ascending=False)
    resStr = str()
    resStr += "votre groupe "+nom1+" evalue beaucoup de film de "+str(res.index[0])+" c'est "+str(res.iloc[0])+" de plus que le groupe "+nom2+"\n"
    resStr += "votre groupe "+nom1+" evalue peu de film de "+str(res.index[-1])+" c'est "+str(res.tail(1).iloc[0])+" en moins que le groupe "+nom2+"\n"
    return resStr


# On veut aussi comparer les stats du types notes, pourquoi pas faires des violin

def EvalParGenres(BDI, ratings_small, links_small, axs, nom='BDI'):
    '''
        pour 1 groupe donne la répartitions des notes par genres de films évalué
    '''
    GRP_DfGenres = GroupeParGenres(BDI)
    ids_par_genres = {}
    Notes_par_genres = {}
    stats_par_genres = {}
    for g in GRP_DfGenres.columns.drop('id'):
        ids_par_genres[g] = GRP_DfGenres.loc[GRP_DfGenres[g] == 1, 'id'].tolist()
        Notes_par_genres[g] = RatingsFromMovies(ids_par_genres[g],ratings_small, links_small)
        stats_par_genres[g] = Notes_par_genres[g]['rating'].describe()
    axs.set_title("répartion des notes des films du groupe {}".format(nom))
    data = [Notes_par_genres[key]['rating'] for key in Notes_par_genres.keys()]
    pos = range(len(Notes_par_genres))
    axs.violinplot(data, pos, widths=0.7, showmeans=True, showextrema=True, showmedians=True)
    axs.set_xticks(pos, Notes_par_genres.keys())
    return stats_par_genres


def CompareEvalGenres(GRP, GRP2, ratings_small, links_small, nom1='BDI', nom2='BDI2'):
    '''
        Compare les EvalParGenres
    '''
    fig, axs = plt.subplots(2, 1, figsize=(18, 6), sharey=False)
    stats = EvalParGenres(GRP, ratings_small, links_small, axs[0], nom1)
    stats2 = EvalParGenres(GRP2, ratings_small, links_small, axs[1], nom2)
    locSave = "../img/CompareEvalGenre.png"
    fig.savefig(locSave)    
    meilleurG= []
    meilleurG2 = []
    diff = []
    for g in stats:
        meilleurG.append((g,stats[g][1]))
        meilleurG2.append((g,stats2[g][1]))
        diff.append((g,stats[g][1]-stats2[g][1]))
    max_MeansDiff = max(diff, key=lambda x: x[1])
    max_Means = next(filter(lambda t: t[0] == max_MeansDiff[0], meilleurG))
    min_MeansDiff = min(diff, key=lambda x: x[1])
    min_Means = next(filter(lambda t: t[0] == min_MeansDiff[0], meilleurG))
    resStr = str()
    resStr = "votre groupe "+nom1+" aime particulierement les films de "+str(max_MeansDiff[0])+" attribue une note moyenne de "+str(max_Means[1])+" c'est "+str(max_MeansDiff[1])+"de plus que le groupe "+nom2+"\n"
    resStr += "votre groupe "+nom1+" n'aime pas particulierement les films de "+str(min_MeansDiff[0])+" attribue une note moyenne de "+str(min_Means[1])+" c'est "+str(min_MeansDiff[1])+"de moins que le groupe "+nom2+"\n"
    return resStr


def RecapGenre(Ui, movies_metadata, links_small, ratings_small):
    '''
        appel tout ce qu'il faut pour l'implémentation sur le site,
        c'est à dire:
            méthode hybride
            Clusters Movies
            indiication du groupes
            Compare les genres évalué
            Compare les évaluations par genres
    '''
    mat =  pd.read_csv('../archive/the-movies-dataset/predUser_MovieSVD.csv', index_col=0)
    mat.columns = np.int64(mat.columns[:].values)
    Hyb = hybride(mat, 'average', 10, 6)
    BDI, tabGrp = ClustersMovies(Hyb, 6, movies_metadata, links_small, ratings_small)
    
    res = str()
    ind,res = VotreGroupe(Ui,BDI, tabGrp)
    res += CompareSelectGenres(BDI[ind], movies_metadata, ratings_small, str(ind+1), "Global")
    res += CompareEvalGenres(BDI[ind],movies_metadata,ratings_small,links_small, str(ind+1), "Global")
    return res
#print(RecapGenre(1, movies_metadata, links_small, ratings_small))

