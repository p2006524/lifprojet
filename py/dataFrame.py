import pandas as pd
import numpy as np

#supprime 3 lignes qui sont nuls
def clean(movies_metadata):
    clean = ["1997-08-20", "2012-09-29", "2014-01-01"]
    movies_metadata = movies_metadata[~movies_metadata['id'].isin(clean)]
    movies_metadata.loc[:, 'id'] = movies_metadata['id'].astype(int)
    return movies_metadata

#filtre que les données interpretable, au moins noté une fois
def cleanAnnalyse(links_small, ratings_small):
    clean2 = links_small[~links_small['movieId'].isin(ratings_small['movieId'])]
    links_small2 = links_small[~links_small['movieId'].isin(clean2['movieId'])]
    return links_small2

def infoFromId(Movies, movies_metadata, links_small):
    tmdbId = []
    fltr = links_small[links_small['movieId'].isin(Movies)]
    tmdbId = fltr['tmdbId']
    return movies_metadata[movies_metadata['id'].isin(tmdbId)]

def RatingsFromMovies(Movies, ratings_small, links_small):
    idRating = []
    fltr = links_small[links_small['tmdbId'].isin(Movies)]
    idRating = fltr['movieId']
    return ratings_small[ratings_small['movieId'].isin(idRating)]

#Retourne de façon unique tout les genres possibles
def UniqueGenres(movies_metadata):
    vrakGenre = movies_metadata['genres'][:]
    listGenre = []
    res = []
    for i in vrakGenre:
        listGenre += genresVal(i)
    listUniqueG = list(set(listGenre))
    return listUniqueG

#fonction pour retrouver tous les genres d'un film dans la base de film donnée
def genresVal(val) :
    val = val.split("'name': '") # on split la chaine pour retrouver tous les genres
    del val[0]
    genre = ""
    genres = []
    for i in val : #parcourir le tableau de noms de genres
        for j in i :
            if j == "'":
                break
            genre += j
        """ print(genre) """
        genre = genre.replace(' ', '-')
        genres.append(genre) #on ajoute je genre trouve au tableau de genres
        genre = ""
    return genres

#Fait des groupes par genres
def GroupeParGenres(movies_metadata):
    GrpMovieParGenres = movies_metadata[['genres', 'id']].explode('genres')
    GrpMovieParGenres['genres'] = GrpMovieParGenres['genres'].apply(genresVal)
    uG = UniqueGenres(movies_metadata)
    for g in uG:
        GrpMovieParGenres[g] = GrpMovieParGenres['genres'].apply(lambda x: 1 if g in x else np.nan)
    GrpMovieParGenres = GrpMovieParGenres.drop('genres', axis=1)
    return GrpMovieParGenres

def VartoName(dico, Variable):
    for nom in dico:
        if dico[nom] is Variable:
            return nom
