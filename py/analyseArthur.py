import pandas as pd
import numpy as np
from system import *
import sys
import json
import ast
from surprise import Reader , Dataset , KNNBasic
import csv
#from tinydb import TinyDB, Query
from analyseAllan import mainTrouverGenre , genreTrouver
#from FiltrageCollaboratif import idFromUserRating , infoFromId , StatUser , ComprendreBudget
#from analysePE import id_links_Par_Titre , titre_Par_ID_Film , join_Two_File , meilleur_Film 
from filtrage_actif import *
from dataFrame import *
from recherche_dans_bd import * 
from AnalyseData import *

#chemin vers archive
location = "../archive/the-movies-dataset/"

# md = pd.read_csv(CheminUnFichier("imdb.csv"))
# mov = pd.read_csv(CheminUnFichier("movies_metadata.csv"), low_memory=False)
# df=md
# gen = md
#Liste sans les valeurs avec des nan
#df = df[df["rating"].notnull()]
# gen['genre'] = gen['genre'].fillna('Pas renseigné')
# gen['rating'] = gen['rating'].fillna(0)

#fonction qui tri dans une base la colonne avec le x premiers
def lesPremiers(base,colonne,x):
    sorted_d = sorted(base[colonne].items(), key=lambda item: item[1], reverse=True)
    s = [sorted_d[i] for i in range(x)]
    return tuple(s)

#fonction qui tri dans une base la colonne avec le x derniers
def lesDerniers(base,colonne,x):
    sorted_d = sorted(base[colonne].items(), key=lambda item: item[1], reverse=False)
    s = [sorted_d[i] for i in range(x)]
    return tuple(s)

#fonction qui tri dans une base la colonne genre avec le genre cherché avec le x premiers
def lesPremiersPrGenre(base,legenre,colonneTri,x):
    x=int(x)
    lesgenre=base[base['genre'].str.contains(legenre)].sort_values(by=colonneTri)
    return list(lesgenre['title'].reset_index(drop=True).head(x))    
    #return legenre.loc[:, 'title']


def filmsParGenre(df, genre, n):
    result = []
    for i, row in df.iterrows():
        if genre in row['genres']:
            result.append(row['title'])
        if len(result) == n:
            break
    return result

#fonction qui renvoi une colonne sous forme de liste
def dfToList(base):
    l = list()
    s = str()
    for i in base:
        l+=str(i).replace('[',"").replace(']',"").replace(", ","").split("'")
    l = list(set(l))
    del l[0]
    return l

#fonction python qui demande dans le terminal de donner certaine info
def recommandationBasique(base):
    justeGen=dfToList(gen['genre'])
    print("Parmit ces genres ",justeGen)
    genre = str(input('Choisir un genre de film :'))
    x = int(input('Choisir un nombre de film :'))
    tri = str(input('Choisir sur quelle colonne trier (ex vote , rating):'))
    return lesPremiersPrGenre(base,genre,tri,x)

#extraire seulement une partie d'une colonne
def extract_names(json_string,colonneVoulu):
    if pd.isna(json_string):
        return []
    try:
        data = json.loads(json_string.replace("'",'"'))
    except json.JSONDecodeError:
        return []
    if isinstance(data, list):
        return [d.get(colonneVoulu, "") for d in data]
    if isinstance(data, dict):
        return [data.get(colonneVoulu, "")]
    return []

#extraire seulement une partie d'une colonne
def extractForColumn(base,colonne,colonneVoulu):
    base[colonne] = base[colonne].fillna('')
    base[colonne] = base[colonne].apply(lambda x: extract_names(x,colonneVoulu))

#écrire une nouvelle ligne dans un csv
def writeInCsv(userId , movieId , rating , timestamp):
    ratings_small= pd .read_csv(location+"ratings_small.csv")
    newLine=len(ratings_small)+1
    ajouter = pd.DataFrame([(userId,movieId,rating,timestamp)], columns=['userId', 'movieId','rating' , 'timestamp'])
    ratings_small = pd.concat([ratings_small,ajouter], ignore_index=True)
    ratings_small.to_csv("../archive/the-movies-dataset/ratings_small.csv", index=False)

# def joinFiles(listeID, movie_metadata_df):
#     print(listeID)
#     listeID[1] = listeID[1].astype(str)
#     movie_metadata_df['id'] = movie_metadata_df['id'].astype(str)

#     merged_df = pd.merge(left=listeID, right=movie_metadata_df, left_on='tmdbId', right_on='id')

#     merged_df['title'] = merged_df['title'].fillna('').apply(lambda x: x.encode('unicode_escape').decode('utf-8')).str.replace("'",'"')
#     merged_df['overview'] = merged_df['overview'].fillna('').apply(lambda x: x.encode('unicode_escape').decode('utf-8')).str.replace("'",'"')
#     merged_df['vote_average'] = merged_df['vote_average'].astype(str).str.replace("'",'"')

#     data = merged_df.to_dict(orient='records')

#     return data

#récup seuleme,t les lignes de la DF voulu (ici par rapport a une liste de film)
def filtreTitre(listeTitre,df):
    df2 = df[['title', 'overview', 'vote_average']].copy()
    df2['vote_average'] = df2['vote_average'].fillna(-1.0)

    df2['title'] = df2['title'].fillna('').apply(lambda x: x.encode('unicode_escape').decode('utf-8')).str.replace("'",'"')
    df2['overview'] = df2['overview'].fillna('').apply(lambda x: x.encode('unicode_escape').decode('utf-8')).str.replace("'",'"')
    df2['vote_average'] = df2['vote_average'].astype(str).str.replace("'",'"')

    df_filtree = df2[df2['title'].isin(listeTitre)]

    data = df_filtree.to_dict(orient='records')
    return data

#renvoi le maximun id dans movies metadata
def maxId():
    df=pd.read_csv(CheminUnFichier("movies_metadata.csv"),low_memory=False)
    liste = dfToList(df["id"])
    max_element = max(int(i) for i in liste if i.isdigit())
    return max_element

#renvoi le maximun de colonne dans links small
def maxLinks(colonne):
    df=pd.read_csv(CheminUnFichier("links_small.csv"),low_memory=False)
    liste = dfToList(df[colonne])
    max_element = max(int(i) for i in liste if i.isdigit())
    return max_element

#ajouter une nouvelle ligne dans movies metadata
def newLineMovies(title,desc,listegenre):
    genre=transforme_format_df(liste_ID_genre(listegenre))
    maxa=maxId()+1
    df=pd.read_csv(CheminUnFichier("movies_metadata.csv"),low_memory=False)
    nouvelle_ligne = {'id':maxa,'title': title , 'overview': desc , 'genres':genre }
    df = df.append(nouvelle_ligne, ignore_index=True)
    df.to_csv(CheminUnFichier("movies_metadata.csv"),index=False)
    return maxa

#ajouter une nouvelle ligne dans links small
def newLineLinks(id):
    maxa=maxLinks('imdbId')+1
    maxid=maxLinks('movieId')+1
    df=pd.read_csv(CheminUnFichier("links_small.csv"),low_memory=False)
    nouvelle_ligne = {'movieId':maxid,'imdbId': maxa, 'tmdbId':id}
    df = df.append(nouvelle_ligne, ignore_index=True)
    df.to_csv(CheminUnFichier("links_small.csv"),index=False)

#redéfinition en attendant bug
# ratings_small = pd.read_csv(location+"ratings_small.csv")
# links_small = pd.read_csv(location+"links_small.csv")
# movies_metadata = pd.read_csv(location+"movies_metadata.csv", low_memory=False)

ratings_small = pd.read_csv(CheminUnFichier("ratings_small.csv"))
links_small = pd.read_csv(CheminUnFichier("links_small.csv"))
movies_metadata = pd.read_csv(CheminUnFichier("movies_metadata.csv"),low_memory=False)

clean = ["1997-08-20", "2012-09-29", "2014-01-01"]
movies_metadata = movies_metadata[~movies_metadata['id'].isin(clean)]
movies_metadata['id'] = movies_metadata['id'].apply(int)

if __name__=='__main__':
    functionName = sys.argv[1]
    if functionName == 'top':
        liste=filmsParGenre(movies_metadata,sys.argv[2],int(sys.argv[3]))
        for i in range(len(liste)):
            print(liste[i])
    # elif functionName == 'lesPremiersPrGenre':
    #     liste=lesPremiersPrGenre(sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
    #     for i in range(len(liste)):
    #         print(liste[i])
    # elif functionName == 'dfToList':
    #     print(dfToList(gen['genre']))
    elif functionName == 'listeUtilisateur':
        liste = dfToList(ratings_small['userId'])
        liste = list(map(int, liste))
        liste.sort()
        print(liste)
    elif functionName == 'extractGenre':
        extractForColumn(movies_metadata,'genres','name')
        print(dfToList(movies_metadata['genres']))
    elif functionName == 'idFromUserRating' :
        #print(sys.argv[2])
        listeid=idFromUserRating(int(sys.argv[2]),0,ratings_small)
        ok=infoFromId(listeid,movies_metadata,links_small)['title']
        print(dfToList(ok))
    elif functionName == 'liste':
        dff = movies_metadata[['title', 'overview', 'vote_average']].copy()
        dff['title'] = dff['title'].fillna('').apply(lambda x: x.encode('unicode_escape').decode('utf-8')).str.replace("'",'"')
        dff['overview'] = dff['overview'].fillna('').apply(lambda x: x.encode('unicode_escape').decode('utf-8')).str.replace("'",'"')
        dff['vote_average'] = dff['vote_average'].astype(str).str.replace("'",'"')

        data = dff.to_dict(orient='records')

        print(json.dumps(data)) 
    elif functionName == 'id_links_Par_Titre':
        print(id_links_Par_Titre(sys.argv[2],links_small,movies_metadata))
    elif functionName=='detailliste':

        dff=liste_detail_flim_note(int(sys.argv[2]),ratings_small,links_small)
        dff= dff[['title','overview','vote_average','rating']].copy()
        dff['title'] = dff['title'].fillna('').apply(lambda x: x.encode('unicode_escape').decode('utf-8')).str.replace("'",'"')
        dff['overview'] = dff['overview'].fillna('').apply(lambda x: x.encode('unicode_escape').decode('utf-8')).str.replace("'",'"')
        dff['vote_average'] = dff['vote_average'].astype(str).str.replace("'",'"')
        dff['rating'] = dff['rating'].astype(str).str.replace("'",'"')

        data = dff.to_dict(orient='records')
        print(json.dumps(data))

    elif functionName== 'writeInCsv':
        writeInCsv(int(sys.argv[2]),int(sys.argv[3]),float(sys.argv[4]),int(sys.argv[5]))
    elif functionName=='plot':
        StatUser(int(sys.argv[2]),ComprendreBudget,ratings_small,10,movies_metadata,links_small)
    elif functionName=='recommandation':
        type = sys.argv[2]
        if type == 'KNN':
            liste_meilleur = meilleur_Film(int(sys.argv[3]),entrainne_KNNBasic(ratings_small),movies_metadata,ratings_small,links_small)
            list = []
            for i in range(10):
                list.append(titre_Par_ID_Film(liste_meilleur[0][i][1],join_Two_File(links_small,movies_metadata)))
            data = filtreTitre(list,movies_metadata)
            print(json.dumps(data))
        elif type == 'SVD':
            liste_meilleur = meilleur_Film(int(sys.argv[3]),entrainne_SVD(ratings_small),movies_metadata,ratings_small,links_small)
            list = []
            for i in range(10):
                list.append(titre_Par_ID_Film(liste_meilleur[0][i][1],join_Two_File(links_small,movies_metadata)))
            data = filtreTitre(list,movies_metadata)
            print(json.dumps(data))
        elif type == 'SIM':
            liste_meilleur = RecoSimFinal(int(sys.argv[3]),movies_metadata, cleanAnnalyse(links_small, ratings_small) , ratings_small)
            list = []
            for i in range(10):
                list.append(titre_Par_ID_Film(liste_meilleur[i][1],join_Two_File(links_small,movies_metadata)))
            data = filtreTitre(list,movies_metadata)
            print(json.dumps(data))
    elif functionName=='nouveauFilm':
        #mainTrouverGenre()
        liste=genreTrouver(sys.argv[3])
        id=newLineMovies(sys.argv[2],sys.argv[3],liste)
        newLineLinks(id)
        liste = str(liste)
        print(liste)
    elif functionName=='statUser':
        print(RecapGenre(int(sys.argv[2]),movies_metadata, links_small, ratings_small))

