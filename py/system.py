import os

def recupDossiersEtFichiers(path):#permet de recupérer les dossier et fichier dans un dosier
    if not os.path.exists(path):
        return False, [], []
    items = os.listdir(path)
    dossiers = [item for item in items if os.path.isdir(os.path.join(path, item))]
    fichiers = [item for item in items if os.path.isfile(os.path.join(path, item))]
    return True, dossiers, fichiers

def elementResembleListe(liste, element):
    """
    permet de savoire si il y a au moins un élement dans une liste
    """
    return any(element in x for x in liste)

def ValeurElementResemblant(liste, element):
    """
    permet de retourner la valeur complete d'une liste a partir d'un mot clé
    """
    for x in liste:
        if element in x:
            return x

def trouvePath(NomFichierDossier: str):
    """
    Recherche en profondeur des fichiers à partir d'un mot-clé.
    Retourne la liste des chemins absolus des fichiers correspondants.
    
    Exemple : pour trouver le fichier `data.csv`, vous pouvez fournir le mot-clé `dat`.
    Note : Il est conseillé de fournir le mot-clé le plus précis possible.
    """
    dataDossier = []
    dataFichier = []
    for chemin, dossier, fichier in os.walk('..'):
        for unDossier in dossier:
            if NomFichierDossier in unDossier:
                dataDossier.append(os.path.join(chemin, unDossier))
        for unFichier in fichier:
            if NomFichierDossier in unFichier:
                dataFichier.append(os.path.join(chemin, unFichier))
    return dataFichier , dataDossier

def CheminUnFichier(nomFichier):
    """
    permet de trouver un seul fichiers parmis toutes les bases de donné
    """
    nomPath = nomFichier.split("/")
    # print(nomPath)
    # print(nomPath[-1])
    (fichiers, dossiers) = trouvePath(nomPath[-1])
    if len(nomPath) > 1:
        nomPathOnly = nomPath[0: len(nomPath)]
        # print(fichiers)
        # print("fin de la fonction")
        return os.path.join(ValeurElementResemblant(fichiers, nomPath[0]))
    # print("fin de la fonction")
    return fichiers[0]

def cheminFichierDansDossier(nomDossier, nomFichier : str =""):
    """
    permet de charger tout les fichier ou seulement ceux qui ressemble a nomfichier.
    """
    (pathsFichiers , pathsDossiers) = trouvePath(nomDossier)
    pathsFichiers = []
    if pathsDossiers == [] :
        print("aucun dossier de se nom :" + nomDossier)
    for dossiers in pathsDossiers:
        pathsFichiers.extend([os.path.join(dossiers, f) for f in os.listdir(dossiers) if os.path.isfile(os.path.join(dossiers, f))]) 
        # ici on ne fait pas un append car on ne veut pas avoir deds liste de liste de fichiers juste une liste
    return pathsFichiers

#print(cheminFichierDansDossier("movies01"))
