# from analyse import *
from system import *

import numpy as np
import pandas as pd
from surprise import Reader, Dataset, accuracy, KNNBasic,SVDpp
from surprise.model_selection import train_test_split
from surprise.prediction_algorithms import SVD 
from surprise.model_selection import cross_validate
from sklearn.preprocessing import MultiLabelBinarizer
import multiprocessing
import threading
from tqdm import tqdm
import queue
import time

import contextlib

# ratings_df = pd.read_csv(CheminUnFichier('data')) Pour lire un fichier .csv avec pandas
# reader = Reader(rating_scale=(1, 5))  Definir l'échelle de rating
# data = Dataset.load_from_df(ratings_df, reader)  Chargement des données dans le dataset
# train_data, test_data = train_test_split(data, test_size=0.25) Séparation des données en jeu d'entraînement et jeu de test
# Entraînement du modèle
# algo = SVD()
# algo.fit(train_data)
# predictions = algo.test(test_data)  Prédiction des notes pour les utilisateurs

"""

"""

class ThreadResult:
    def __init__(self):
        self.result = None

def execute_tasks(tasks):
    threads = []
    results = [None] * len(tasks)
    for i, (func, args) in enumerate(tasks):
        thread = threading.Thread(target=execute_func, args=(func, args, i, results))
        thread.start()
        threads.append(thread)
    print("fin du lancement des threads")
    i = 0
    for thread in threads:
        i = i + 1
        print(f"fin du tread {i}")
        thread.join()
    return results

def execute_func(func, args, i, results):
    result = func(*args)
    results[i] = result

def lance_thread(func, arg, i):
    """
    permet de lancer un thread sur une fonction avec ces arguments
    /!\ il faut absolument récupérer le resultat donc appeler recupere_resultat_thread()
    """
    result = []
    thread = threading.Thread(target = execute_func, args = (func, arg))
    thread.thread_result = ThreadResult()
    thread.start()
    return thread

def recupere_resultat_thread(thread):
    """
    permet de recupérer le resultat du thread
    retourne le resultat de ce thread
    """
    thread.join()
    return thread

def join_Two_File (link_df, movie_metadata_df): #====================================================================a mettre dans un autre fichier
    """
    permet de joindre fichier deux fichiers du type links et movie_metadata
    retourne une df avec id de links, id de movie_metadata, et les titres des films (pas l'original)
    """
    link_df['tmdbId'] = link_df['tmdbId'].fillna(0).astype(int)
    link_df['tmdbId'] = link_df['tmdbId'].astype(str)
    movie_metadata_df['id'] = movie_metadata_df['id'].astype(str)
    merged_df = pd.merge(left=link_df, right=movie_metadata_df, left_on='tmdbId', right_on='id')
    return merged_df[['movieId', 'tmdbId', 'title']]

def id_Par_Nom_Film(nom_Du_Film : str, df): #====================================================================a mettre dans un autre fichier
    """
    permet de trouver l'identifiant d'un film a partir de son titre. Compatible avec movies/movies01 ou the-movies-dataset
    attention l'id est celui de movie_metadata
    """
    if df.empty or 'title' not in df.columns:
        # print("df.empty or 'title' not in df.columns")
        return None
    matching_title = df['title'].str.contains(nom_Du_Film, na=False)
    if not matching_title.any():
        # print("not matching_title")
        return None
    movie_id = df.loc[df['title'].str.contains(nom_Du_Film), 'id'].iloc[0]
    return movie_id

def id_links_Par_Titre(nom_Du_Film: str, Df_links=None, Df_title=None): #====================================================================a mettre dans un autre fichier
    """
    permet de trouver l'identifiant d'un film a partir de son titre. Compatible avec movies/movies01 ou the-movies-dataset
    attention l'id est celui de links
    """
    if Df_links is None:
        Df_links = pd.read_csv(CheminUnFichier('the-movie/links_sm'), usecols=['movieId', 'tmdbId'])
    if Df_title is None:
        Df_title = pd.read_csv(CheminUnFichier('the-movie/movies_meta'), usecols=['title', 'id'],low_memory=False)
    if not {'movieId', 'tmdbId'}.issubset(set(Df_links.columns)) or not {'title', 'id'}.issubset(set(Df_title.columns)):
        return None
    matching_id = Df_links.loc[Df_links['tmdbId'] == float(Df_title.loc[Df_title['title'] == nom_Du_Film, 'id'].iloc[0])].iloc[0]
    return int(matching_id['movieId'])

def no_matching_id (id : int, df):
    """
    permet de vérifier qu'un id est bien dans une df
    """
    matching_id = df['movieId'] == id
    if not matching_id.any():
        return True
    else:
        return False

def titre_Par_ID_Film(id_du_film: int, df): #====================================================================a mettre dans un autre fichier
    """
    permet de trouver le titre d'un film a partir de son identifiant. 
    Compatible avec movies/movies01 ou the-movies-dataset
    /!\ l'id doit être celui de movie_metadata
    retourne le titre du film
    """
    if df.empty:
        # print("df.empty")
        return None
    if 'movieId' not in df.columns:
        # print("colone manquante")
        return None
    matching_id = df['movieId'] == id_du_film
    if not matching_id.any():
        # print("not matching_id")
        return None
    movie_title = df.loc[matching_id, 'title'].iloc[0]
    return movie_title

def adapte_Data_Pour_Algo(ratings_df): 
    """
    fonction interne a ne pas utiiser en externe
    permet d'addapter la df ratings_df pour les algos de prédiction
    return la df du bon "format"
    """
    ratings_df = ratings_df.iloc[:, :3]
    ratings_df = ratings_df.rename(columns={'user': 'userId', 'item': 'movieId', 'rating': 'rating'}) # on renome pour être compatible avec les fonction de soup
    reader = Reader(rating_scale=(1, 5)) # Definir l'échelle de rating
    return Dataset.load_from_df(ratings_df, reader) # Chargement des données dans le dataset
    

def entrainement_Model(ratings_df = None, model = SVD):
    """
    permet d'entrainner un algo de prediction,
    fonctionne avec au moins SVD et KNNbasic
    return l'ago qui est utiliser pour prédire une note
    """
    if ratings_df is None:
        ratings_df = pd.read_csv(CheminUnFichier('the-movie/ratings_sm'))
    data = adapte_Data_Pour_Algo(ratings_df)
    algo = model()
    with open('log.txt', 'w') as f:
        with contextlib.redirect_stdout(f):
            algo.fit(data.build_full_trainset())
    return algo

def evaluation_Model(ratings_df = None, model = SVD):
    """
    permet d'évaluer la performance d'un algo de prediction,
    compatible avec les algos de surprices
    retourne le rmse soit une note entre 0 et 1 qui détermine la présision de l'ago
    """
    if ratings_df is None :
        ratings_df = pd.read_csv(CheminUnFichier('the-movie/ratings_sm'))
    # charger les données dans un dataset surprise
    data = adapte_Data_Pour_Algo(ratings_df)
    # diviser le dataset en un ensemble de formation et un ensemble de test
    trainset, testset = train_test_split(data, test_size=0.25)
    #print("créer un modèle et l'entraîne sur l'ensemble de formation")
    model = model()
    model.fit(trainset)
    #print(" faire des prédictions sur l'ensemble de test ")
    predictions = model.test(testset)
    # calculer la RMSE sur l'ensemble de test
    rmse = accuracy.rmse(predictions)
    return rmse


def predire_Note_Utilisateur(titre_Film, num_Utili, movie_metadata_df = None,ratings_df = None, links_df = None, algo = None):
    """
    permet de predire une note pour un film, il faut soit l'id correspondant a l'id de links ou ratings
    /!\ pas l'id de movies_metadata
    retourne une liste d'information sur le film, dans ce format :
    user:(l'utilisateur)  item:(l'id du film)  r_ui = None   est = (la note predite)   {'was_impossible': (booléen qui est faux si on a pu prédire)}
    """
    if ratings_df is None:
        #print("pas de df renseigner on prend movies01/ratings_sm")
        ratings_df = pd.read_csv(CheminUnFichier('movies01/ratings_sm'))
    if algo is None:
        #print("algo de prediction pas renseignier on va le créé avec SVD")
        algo = entrainement_Model(ratings_df)
    if movie_metadata_df is None:
        movie_metadata_df = pd.read_csv(CheminUnFichier('movies01/movies_meta'), low_memory=False)
    if type(titre_Film) == str:
        links_df = pd.read_csv(CheminUnFichier('movies01/links.csv'))
        id_Movie = id_links_Par_Titre(nom_Du_Film = titre_Film, Df_links = links_df, Df_title = movie_metadata_df)
    else:
        id_Movie = titre_Film
    prediction = algo.predict(num_Utili, id_Movie)
    return prediction


def meilleur_Film(num_Utili, algo, data=None, ratings_df=None, links_df=None, sort = True):
    if data is None:
        data = pd.read_csv(CheminUnFichier('the-movies/movies_metadata'), low_memory=False)
    if ratings_df is None:
        ratings_df = pd.read_csv(CheminUnFichier('the-movies/ratings_sm'))
    if links_df is None:
        links_df = pd.read_csv(CheminUnFichier('the-movies/links_sm'))
    if links_df is None:
        links_df = pd.read_csv(CheminUnFichier('the-movies/links_sm'))
    liste_note = []
    liste_non_precis = []
    liste = []
    for i in links_df['movieId']:
        liste.append(predire_Note_Utilisateur(i, num_Utili, data, ratings_df, links_df, algo))
    for i in liste:
        if i[4] ['was_impossible']:
            liste_non_precis.append((i.est, i.iid))
            if sort:
                liste_non_precis.sort(key=lambda x: x[0], reverse=True) 
        else:
            liste_note.append((i.est, i.iid))
            if sort:
                liste_note.sort(key=lambda x: x[0], reverse=True)
    retour = [None] * 2
    retour [0] = liste_note
    retour [1] = liste_non_precis
    return retour


def meilleur_Film2(num_Utili, algo, data=None, ratings_df=None, links_df=None, sort = True):
    '''
        version pour adapter aux fonction de Valtin
    '''
    if links_df is None:
        links_df = pd.read_csv(CheminUnFichier('movies01/links_sm'))
    #print("c'est lu")
    liste_note = []
    liste_non_precis = []
    # join = join_Two_File(links_df, data)

    liste = []
    for i in links_df['movieId']:     #links_df et rating n'ont pas le même nombre de film
        liste.append(predire_Note_Utilisateur(i, num_Utili, data, ratings_df, links_df, algo))
    for i in liste:
        if i[4] ['was_impossible']:
            liste_non_precis.append((i.est, i.iid))
            liste_note.append(np.nan)
            if sort:
                liste_non_precis.sort(key=lambda x: x[0], reverse=True) 
        else:
            liste_note.append(i.est)
            if sort:
                liste_note.sort(key=lambda x: x[0], reverse=True)
    retour = [None] * 2
    retour [0] = liste_note
    retour [1] = liste_non_precis
    return retour



def entrainne_KNNBasic(ratings_df = None):
    if ratings_df is None:
        ratings_df = pd.read_csv(CheminUnFichier('movies/ratings_sm'))
    return entrainement_Model(ratings_df, model=KNNBasic)

def entrainne_SVD(ratings_df = None):
    if ratings_df is None:
        ratings_df = pd.read_csv(CheminUnFichier('the-movie/ratings_sm'))
    return entrainement_Model(ratings_df, model=SVD)






# def meilleur_Film(num_Utili, algo, data=None, ratings_df=None, links_df=None, num_threads=4):
#     if data is None:
#         data = pd.read_csv(CheminUnFichier('movies01/movies_metadata'), low_memory=False)
#     if ratings_df is None:
#         ratings_df = pd.read_csv(CheminUnFichier('movies01/ratings_sm'))
#     if links_df is None:
#         links_df = pd.read_csv(CheminUnFichier('movies01/links_sm'))
        
#     # Diviser la liste des films en sous-listes pour chaque thread
#     num_films = len(links_df)
#     films_per_thread = (num_films + num_threads - 1) // num_threads  # arrondi supérieur
#     film_lists = [links_df[i:i + films_per_thread] for i in range(0, num_films, films_per_thread)]
    
#     # Lancer un thread pour chaque sous-liste de films
#     threads = []
#     result = [None] * num_threads
#     print (result)
#     for i, (films) in enumerate(film_lists):
#         threads.append(lance_thread(meilleur_Film_thread, arg=(num_Utili, algo, data, ratings_df, films)))
    
#     # Fusionner les résultats de chaque thread
#     liste_note = []
#     liste_non_precis = []
#     # Attendre que tous les threads se terminent

#     for thread in threads:
#         result = recupere_resultat_thread(thread = thread)
#         liste_note+= result [0]
#         liste_non_precis+= result [1]
    
#     liste_note.sort(key=lambda x: x[0], reverse=True)
#     liste_non_precis.sort(key=lambda x: x[0], reverse=True)
#     return (liste_note, liste_non_precis)


# def meilleur_Film_thread(num_Utili, algo, data, ratings_df, links_df):
#     liste_note = []
#     liste_non_precis = []
#     for film in links_df['movieId']:
#         if film is not None :
#             note = predire_Note_Utilisateur(film, num_Utili, data, ratings_df, algo)
#             if note[4]['was_impossible']:
#                 liste_non_precis.append((note.est, film))
#             else:
#                 liste_note.append((note.est, film))
#     liste_note.sort(key=lambda x: x[0], reverse=True)
#     liste_non_precis.sort(key=lambda x: x[0], reverse=True)
#     thread_result = ThreadResult()
#     thread_result.result = [liste_note, liste_non_precis]
#     return thread_result




#arthuramitencom Algo = entrainne_KNNBasic()
#arthuramitencom print("fin de l'entrainement de KNNBasic")
#arthuramitencom print(meilleur_Film(1, Algo))

def liste_Film_wrong_id (links):
    liste_retour = []
    for elem in links:
        if not(elem[2].isdigit()):
            liste_retour.append(elem [0])
    return liste_retour

def build_feature_matrix(data, features):
    matrices = []
    for feature in features:
        if feature == 'genres':
            mlb = MultiLabelBinarizer()
            genres = data['genres'].apply(lambda x: [d['name'] for d in eval(x)])
            genres_matrix = pd.DataFrame(mlb.fit_transform(genres), columns=mlb.classes_, index=data.index)
            matrices.append(genres_matrix)
        elif feature == 'actors':
            mlb = MultiLabelBinarizer()
            actors = data['cast'].apply(lambda x: [d['name'] for d in eval(x)][:5])
            actors_matrix = pd.DataFrame(mlb.fit_transform(actors), columns=mlb.classes_, index=data.index)
            matrices.append(actors_matrix)
        elif feature == 'directors':
            directors_matrix = pd.get_dummies(data['directors'], prefix='director')
            matrices.append(directors_matrix)
        elif feature == 'languages':
            languages_matrix = pd.get_dummies(data['original_language'], prefix='language')
            matrices.append(languages_matrix)
        elif feature == 'subtitles':
            mlb = MultiLabelBinarizer()
            subtitles = data['spoken_languages'].apply(lambda x: [d['name'] for d in eval(x)])
            subtitles_matrix = pd.DataFrame(mlb.fit_transform(subtitles), columns=mlb.classes_, index=data.index)
            matrices.append(subtitles_matrix)
        elif feature == 'runtime':
            runtime_matrix = data[['runtime']].fillna(data['runtime'].mean())
            matrices.append(runtime_matrix)
        elif feature == 'popularity':
            popularity_matrix = data[['popularity']].fillna(data['popularity'].mean())
            matrices.append(popularity_matrix)
        elif feature == 'release_date':
            release_date_matrix = pd.get_dummies(pd.to_datetime(data['release_date']).dt.year, prefix='year')
            matrices.append(release_date_matrix)
    
    feature_matrix = pd.concat(matrices, axis=1)
    return feature_matrix

def recupere_tout_les_genre(data):
    print(data["genres"])

def recupere_Genre_Par_Id_Film(id, data):
    return data.loc[data['id'] == id, 'genres'].iloc[0]

def calcul_deux_prediction(utilisateur : int):
    data = pd.read_csv(CheminUnFichier('movies/movies_metadata'), low_memory=False)
    links_df = pd.read_csv(CheminUnFichier('movies/links_sm'))
    ratings_df = pd.read_csv(CheminUnFichier('movies/ratings_sm'))
    liste_wrong_id = liste_Film_wrong_id(links_df)
    #print("il y a " + str(len(liste_wrong_id))+ " films qui n'ont pas un bon identifiant" )
    # progress_bar =  [tqdm(total=100, desc="Thread 1"), tqdm(total=100, desc="Thread 2")]
    ##################################################### on a juste a ajouter des lignes pour avoir plus de model que prediction
    algo_SVD = entrainement_Model(ratings_df = None, model = SVD)
    algo_KNNBasic = entrainement_Model(ratings_df = None, model = KNNBasic)
    tasks_predi = [
        (meilleur_Film, (utilisateur, algo_SVD ,data, ratings_df, links_df)),
        (meilleur_Film, (utilisateur, algo_KNNBasic, data, ratings_df, links_df))
    ]
    tasks_eval = [
        (evaluation_Model, (ratings_df, SVD)),
        (evaluation_Model, (ratings_df, KNNBasic))
    ]
    threads = []
    results_predi = [None] * len(tasks_predi)
    results_eval = [None] * len(tasks_eval)
    for i, (func, args) in enumerate(tasks_predi):
        thread = threading.Thread(target=execute_func, args=(func, args, i, results_predi))
        thread.start()
        threads.append(thread)
    for i, (func, args) in enumerate(tasks_eval):
        thread = threading.Thread(target=execute_func, args=(func, args, i, results_eval))
        thread.start()
        threads.append(thread)
    print("fin du lancement des threads")
    i = 0
    for thread in threads:
        i = i + 1
        print(f"fin du tread {i}")
        thread.join()
    # print(results_predi)
    best_predict = []
    if (results_eval [1] > results_eval [0]):
        best_predict = results_predi[1]
        other_predict = results_predi [0]
    else:
        best_predict = results_predi [0]
        other_predict = results_predi [1]
    i = 0
    for i, (vote, film_id) in enumerate(best_predict[1]):
        for other_vote, other_film_id in other_predict[0]:
            if film_id == other_film_id:  # si les identifiants des films sont les mêmes
                best_predict[1][i] = (other_vote, film_id)  # on remplace le vote non précis par le vote de other_predict
    return best_predict
           


def lancer_calcul_prediction(ratings_df = None):
    """
    permet de lancer le calcul de l'algo de prediction,
    On a besion juste des votes des utilisateurs
    return le thread qui fait le calcul 
    /!\ il faut absolument appeler recupere_resultat_thread(thread)
    """
    if ratings_df is None :
        ratings_df = pd.read_csv(CheminUnFichier('movies/ratings_sm'))
    return lance_thread(entrainement_Model, (ratings_df, KNNBasic))









    


# # evaluation_Model(ratings_df, SVDpp)
# # evaluation_Model(ratings_df, SVD)
# data = pd.read_csv(CheminUnFichier('movies/movies_metadata'), low_memory=False)
# links_df = pd.read_csv(CheminUnFichier('movies/links_sm'))
# ratings_df = pd.read_csv(CheminUnFichier('movies/ratings_sm'))
# # evaluation_Model(ratings_df, KNNBasic)
# liste_meilleur = meilleur_Film(5, data, ratings_df ,links_df, KNNBasic)
# # print(liste_meilleur)
# print(titre_Par_ID_Film(liste_meilleur [0] [0] [1], join_Two_File(links_df, data))) # affiche le titre du meilleur film pour l'utilisateur


################################### autre 

# list des models possible : SVD, KNNBasic
# Create a pool with multiple processes
# with multiprocessing.Pool(processes=4) as pool:
#     # Call the three functions in parallel
#     results = [
#         # pool.apply_async(evaluation_Model, (ratings_df, SVDpp)),
#         pool.apply_async(evaluation_Model, (ratings_df, SVD)),
#         pool.apply_async(evaluation_Model, (ratings_df, KNNBasic)),
#         pool.apply_async(meilleur_Film, (1, data, ratings_df ,links_df, SVD)),
#         pool.apply_async(meilleur_Film, (1, data, ratings_df ,links_df, KNNBasic))
#     ]
    
#     # Get the results from the pool
#     results = [r.get() for r in results]
    
#     # Print the results
    
#     print(f"pour le model SVD on a une précision")
#     print(results [0])
#     print("pour le model KNNBasic on a une précision")
#     print(results [1])
#     join = join_Two_File(links_df, data)
#     print("pour le model SVD on a pour les 10 meilleur films ")
#     for i in range(10):
#         print(f"id film = {results[2] [0] [i] [1]} son titre {titre_Par_ID_Film(results[2] [0] [i] [1], join)}")
#     print("pour le model KNNBasic on a pour les 10 meilleur films")
#     for i in range(10):
#         print("id film = "+str (results[3] [0] [i] [1]) + " son titre " + str(titre_Par_ID_Film(results[3] [0] [i] [1], join)))
#     print("pour le model KNNBasic on a pour les 10 pire films")
#     for i in range(10):
#         print("id film = "+str (results[3] [0] [-i] [1]) + " son titre " + str(titre_Par_ID_Film(results[3] [0] [-i] [1], join)))
#     for i in results[3][1]:
#         if titre_Par_ID_Film(i[1], join) is None:
#             print("no matching id")
#         else:
#             print("le film n'a pas une bonne prediction : " + titre_Par_ID_Film(i[1], join))
#        print("il y a "+ str(len(results[3][1])) + " films qui on pas une bonne prediction sur "+ str(len(data ["id"])))





###########################################fonction faite a la main pour apprendre le python et voire se qu'on pourait avoir besion comme fonction

def chargeDonneInDossier(nomDonners : str):
    """
    Permet de charger touts les fichiers d'une base de donner avec le nom du dossier en mode csv
    """
    chemins = cheminFichierDansDossier(nomDonners)
    print(chemins)
    data = [pd.read_csv(elem) for elem in chemins]
    return data

#print(chargeDonneFichier("databaseLoL"))

def chargeDonneDossier(nomTypeDonner : str):
    """
    /!\ non fonctionnel
    permet de charger touts les bases de donné (en csv) d'un certain nom, par exemple movie va charger movie01 movie02 ect ...
    """
    (fichier, dossiers) = trouvePath(nomTypeDonner)
    dossiers = [cheminDossier for cheminDossier in dossiers if 1 == dossiers.count(cheminDossier)]
    return [chargeDonneInDossier(x) for x in dossiers]

# print(chargeDonneDossier("data"))

def fusionDeBase(base1 : str, base2: str):
    """
    /!\ non fonctionnel
    permet de fusionner deux base de donne
    """
    return []

def listeAimerPar(idUtili : int, donners : str):
    """
    /!\ non fonctionnel
    retroune une liste de film avec les utilisateurs qui l'ont aimer
    """
    chargeDonneInDossier(donners)
